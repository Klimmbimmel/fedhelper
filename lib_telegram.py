# -*- coding: utf-8 -*-
def escape_markdown(string: str, v2=False) -> str:
    list_of_chars_to_be_replaced = ['_', '*', '`', '[', ']']

    # Einzeln getestet mit &parse_mode=MarkdownV2; nicht ersetzt $ ' , / : ; ? @
    # siehe: https://en.wikipedia.org/wiki/Percent-encoding#Percent-encoding_reserved_characters
    v2_dict_replace_percent_encoded = {"%": "%25", "!": "\!", "#": "\%23", "&": "%26", "(": "\(", ")": "\)",
                                       "*": "\*", "+": "\%2B", "=": "\=", "[": "\[", "]": "\]"}

    if v2:
        for char in v2_dict_replace_percent_encoded:
            string = string.replace(char, v2_dict_replace_percent_encoded[char])
    else:
        for char in list_of_chars_to_be_replaced:
            string = string.replace(char, "\\" + char)
    return string


def from__to_readable_str(from_dict: dict) -> str:
    return_str = "\n"
    for item in from_dict:
        if item == "is_bot":
            continue
        if item == "username":
            return_str += f"{item}: @{from_dict[item]}\n"
        else:  # first_name, last_name, language_code
            return_str += escape_markdown(str(item)) + ": " + \
                          escape_markdown(str(from_dict[item])) + "\n"
    return return_str


def chat__to_readable_str(from_dict: dict, print_all_members_are_administrators=False) -> str:
    return_str = "\n"
    for item in from_dict:
        if item == "is_bot":
            continue
        elif item == "username":
            return_str += f"{item}: @{from_dict[item]}\n"
        elif item == "all_members_are_administrators":
            if print_all_members_are_administrators:
                return_str += escape_markdown(str(item)) + ": " + \
                              str(from_dict[item]) + "\n"
        else:
            return_str += escape_markdown(str(item)) + ": " + \
                          escape_markdown(str(from_dict[item])) + "\n"
    return return_str
