# -*- coding: utf-8 -*-
from __future__ import annotations

try:
    import lib_spam_federation_helper_bot_textstrings as texts
    from hidden_variables import dating_profile_indicators, dopp_or_sba_smileys, drugspam_indicators, \
        inheritance_indicators, investment_trading_indicators, only_drug_indicators, \
        list_of_dopp_part_starters, list_of_dopp_part_indicators, list_of_dopp_full_indicators
except ModuleNotFoundError:
    import fedhelper_main.lib_spam_federation_helper_bot_textstrings as texts
    from fedhelper_main.hidden_variables import dating_profile_indicators, dopp_or_sba_smileys, drugspam_indicators, \
        inheritance_indicators, investment_trading_indicators, only_drug_indicators, \
        list_of_dopp_part_starters, list_of_dopp_part_indicators, list_of_dopp_full_indicators
    print("\n\n ! lib_spam_federation_helper_bot.py"
          "\n This code should only be running in the unit test files.   !\n\n")

markdown_v1_chars_which_cant_be_urllib_quoted = {"_": "%5F"}  # "*`~|[]" - all have a percent-encoding available


def clean_str_from_markdown_v1_chars(text: str, only_escape: bool = False) -> str:
    for char in markdown_v1_chars_which_cant_be_urllib_quoted:
        if only_escape:
            text = text.replace(char, f"\{char}")
        else:
            text = text.replace(char, markdown_v1_chars_which_cant_be_urllib_quoted[char])
    return text


def read_out_dictionary_file(file_containing_dictionary: str) -> dict:
    """Kreiere spezifische Allowlist. Enhält die, die für die Föderation arbeiten
    Format:     ID: First_Name      dict.keys() = ids, dict.values() = names"""
    dictionary = dict()
    with open(file_containing_dictionary, "r") as file:
        lines_in_file = file.readlines()
        for line in lines_in_file:
            line = line.rstrip()  # nötig um den Zeilenumbruch zu löschen
            if (":" not in line) and line:
                print(f"{texts.console__readout_df_file__file_broken} {file_containing_dictionary}")
                continue
            id_and_name = line.split(sep=":", maxsplit=1)
            try:
                id_ = int(id_and_name[0])
            except ValueError:
                print(f"{texts.console__readout_df_file__line_broken} {file_containing_dictionary}: {id_and_name}")
                continue
            entry = {id_: id_and_name[1]}
            if id_ not in dictionary:
                dictionary.update(entry)
            else:
                print(f"{texts.console__readout_df_file__double_line} {file_containing_dictionary}: {id_and_name}")
    return dictionary


def append_to_dictionary_file(filename_to_write: str, entry: str) -> None:
    with open(filename_to_write, "a") as file:
        file.write("\n" + entry)


def create_fban_message(userid: int, name: str = "", username: str = "", ban_grund: str = "") -> str:
    """Generating message as string, with and without markdown"""
    created_message = texts.user_id_not_banned
    parsed_name = clean_str_from_markdown_v1_chars(name)
    parsed_username = clean_str_from_markdown_v1_chars(username, only_escape=True)
    userid_readable = f"{userid:,}".replace(",", ".")

    if parsed_name:
        created_message += f"\nuser: {parsed_name}"
    if parsed_username:
        created_message += f"\n@{parsed_username}, {userid_readable} `{userid}`"
    else:
        created_message += f"\n{link_to_TG_profil(userid)}, {userid_readable} `{userid}`"
    created_message += f"\n/fban {userid} {clean_str_from_markdown_v1_chars(ban_grund)}"
    created_message += f"\n`/fban {userid}`"
    if ban_grund:
        created_message += f"\n`/fban {userid} {clean_str_from_markdown_v1_chars(ban_grund, only_escape=True)}`"
    return created_message


def create_already_preventive_fbanned_message(userid: int, name: str = "", username: str = "",
                                              ban_grund: str = "") -> str:
    created_message = texts.user_id_is_banned_preventive
    parsed_name = clean_str_from_markdown_v1_chars(name)
    parsed_username = clean_str_from_markdown_v1_chars(username, only_escape=True)
    userid_readable = f"{userid:,}".replace(",", ".")

    if parsed_name:
        created_message += f"\nuser: {parsed_name}"
    if parsed_username:
        created_message += f"\n@{parsed_username}, {userid_readable} `{userid}`"
    else:
        created_message += f"\n{link_to_TG_profil(userid)}, {userid_readable} `{userid}`"
    created_message += f"\n/fban {userid} {clean_str_from_markdown_v1_chars(ban_grund)}"
    created_message += f"\n`/fban {userid} {clean_str_from_markdown_v1_chars(ban_grund, only_escape=True)}`"
    return created_message


def create_already_fbanned_message(userid: int, name: str = "", username: str = "") -> str:
    created_message = texts.user_id_is_banned
    parsed_name = clean_str_from_markdown_v1_chars(name)
    parsed_username = clean_str_from_markdown_v1_chars(username, only_escape=True)
    userid_readable = f"{userid:,}".replace(",", ".")

    if parsed_name:
        created_message += f"\nuser: {parsed_name}"
    if parsed_username:
        created_message += f"\n@{parsed_username}"
    else:
        created_message += f"\n{link_to_TG_profil(userid)}, {userid_readable} `{userid}`"
    return created_message


def create_user_joined_message(userid: int, name: str = "", allowlisted: bool = False,
                               already_allowed: bool = False) -> str:
    created_message = f"Neue\*r Nutzer\*in {clean_str_from_markdown_v1_chars(name)}\n{link_to_TG_profil(userid)}"
    created_message += f"\nID: {userid}, `{userid}`"

    if already_allowed:
        created_message += "\nIst bereits auf Allowlist\n"
        return created_message
    if not allowlisted:
        created_message += "\nMuss voraussichtlich auf Allowlist\n"
    else:
        created_message += "\nAutomatisch auf temporäre Allowlist gesetzt\n"
    created_message += f"\n/add2fedworkerslist {userid}:{clean_str_from_markdown_v1_chars(name)}"
    created_message += f"\n`/add2fedworkerslist {userid}:{clean_str_from_markdown_v1_chars(name, only_escape=True)}`"
    created_message += f"\n`/rereadworkerlist`"
    return created_message


def link_to_TG_profil(userid: int) -> str:
    # Ob der Link funktioniert, hängt von der Privatsphären-Einstellung des Nutzers ab
    return f"[Profil](tg://user?id={userid})"


def get_argument_of_command(text_with_command: str, command: str) -> str | None:
    command_leer = command + " "
    stripped = text_with_command.split(command_leer)
    if len(stripped) == 2:
        if stripped[1]:
            return stripped[1]
    return


def dict_of_users_in_msg_entities(message: dict, invisible_mentions_mode: bool = False) -> (dict, dict):
    """
    :param message: message statt nur message["text"] um die Entities durchzuschauen.
    :param invisible_mentions_mode: Wenn True werden die angepingten Admins herausgefiltert.
    :return users_found: dict mit id:first_name
            tags_detected: dict mit id:list of (here detected) ban reasons
    """
    users_found = dict()
    tags_detected = dict()

    if "entities" in message:
        for item in message["entities"]:
            if "user" not in item:
                # Admins werden offenbar immer als item["type"] == "text_mention" eingebaut
                continue
            if invisible_mentions_mode:
                if "offset" in item:  # normalerweise immer vorhanden
                    try:
                        if message["text"][item["offset"]] == "\u200b":
                            continue
                    except IndexError:
                        # Unicode-Text bewirkt, dass die Offsets nicht stimmen
                        continue
            user__id: int = item["user"]["id"]
            user__first_name: str = item["user"]["first_name"]
            users_found.update({user__id: user__first_name})

            tags_detected.update(check_user_object_for_ban_reasons(item["user"]))

    if invisible_mentions_mode and not users_found:
        # leere user(first_name) können auch hier vorkommen, werden in der Erstellung von der abzuschickenden Nachricht
        # erkannt, daher hier nur der spezielle Parsing-Fall "nix gefunden"
        users_found.update({0: texts.rose_reported_user_is_not_linked__no_newline})

    return users_found, tags_detected


def is_uid_mentioned_and_return_first_and_last_name(message_to_search_for_mentions: dict, id_to_find: int) -> (bool, str, str):
    """
    :return: bool if ID was found + str of user_first_name + str of user_last_name (True + "" + * = Deleted Account)
    """
    last_name_optional = ""
    if "entities" not in message_to_search_for_mentions:
        return False, "", ""
    for entity in message_to_search_for_mentions["entities"]:
        if "user" in entity:
            if entity["user"]["id"] == id_to_find:
                if "last_name" in entity["user"]:
                    last_name_optional = entity["user"]["last_name"]
                return True, entity["user"]["first_name"], last_name_optional
    return False, "", ""


def only_usernames_mentioned__no_user_linked(message: dict) -> bool:
    # um Warnung auszugeben, falls eigentlich ein @username gemeldet wurde,
    #   und daher falsche /fban-Vorlagen gepostet werden könnte, wenn nur die ID der Nachricht hergenommen wird
    # before it was part of dict_of_users_in_msg_entities()
    username_was_mentioned = False
    user_was_linked = False
    only_usernames_were_mentioned = False

    if "entities" in message:
        for item in message["entities"]:
            if item["type"] == "mention":
                # @username im Text (bei Mono nicht)
                # type="text_mention" ist die Entity bei Nutzern ohne @username
                # seltener Spezialfall: Verlinkung von Text mit https://t.me/username bewirkt Entity "text_link"
                # siehe auch: https://core.telegram.org/bots/api#messageentity
                username_was_mentioned = True
                continue
            if item["type"] == "text_mention":
                user_was_linked = True

    if username_was_mentioned and not user_was_linked:
        only_usernames_were_mentioned = True

    return only_usernames_were_mentioned


def check_user_object_for_ban_reasons(user_dict: dict) -> dict[int, set]:
    # 1. Check for dating_profile_indicators
    # 2. Check for dating_profile_fullname_indicators -> can't differentiate -> datingOrPornProfileORspambotAdderProfile
    # 3. Check for only_drug_indicators -> remove 1 or 2
    # if nothing found, check for 4. namingscheme or if not found check for 5. fullnamefirst

    user__id: int = user_dict["id"]
    user__first_name: str = user_dict["first_name"]
    try:
        user__last_name = user_dict["last_name"]
    except KeyError:
        user__last_name = ""
    # Whitespace might be a problem in rare cases, but it is used in logchannel entries the same way
    # It is added so indicators like " girl" will work (" <str>" is used so that str is found as seperate part of the
    # name, and if the last name starts with <str> it can be seen as seperated string within the name - instead of
    # accidentally embedded string like girl in "wtf, omgirl"
    user__full_name = f" {user__first_name} {user__last_name} "

    tags_detected = dict()
    tags_detected.update({user__id: set()})

    if "username" in user_dict:
        if check_username_for_dopp(user_dict["username"]):
            tags_detected[user__id].add("datingOrPornProfile")

    dopp_indicators_cropped = ["oh yes", "Oh Yes"]
    cropped_user__full_name, _ = remove_duplicate_letters_and_trailing_number(user__full_name)
    if any(indicator in cropped_user__full_name for indicator in dopp_indicators_cropped):
        tags_detected[user__id].add("datingOrPornProfile")

    dating_profile_fullname_indicators = [" 𝓘𝓼𝓪𝓫𝓮𝓵𝓵𝓪", " Ｂｉｔｃｈ Ｇｉｒｌ"]  # starting whitespace added intentionally

    if any(indicator in user__full_name for indicator in dating_profile_indicators):
        tags_detected[user__id].add("datingOrPornProfile")
    if any(indicator in user__full_name for indicator in dating_profile_fullname_indicators):
        tags_detected[user__id].add("datingOrPornProfile")

    if "datingOrPornProfile" not in tags_detected[user__id]:
        SPA_found = False
        dopp_because_multiple_SPA_found = False
        for indicator in dopp_or_sba_smileys:
            if indicator in user__full_name:
                if SPA_found:
                    dopp_because_multiple_SPA_found = True
                    break
                SPA_found = True
                if user__full_name.count(indicator) > 1:
                    dopp_because_multiple_SPA_found = True
                    break
        if dopp_because_multiple_SPA_found:
            tags_detected[user__id].add("datingOrPornProfile")
        elif SPA_found:
            tags_detected[user__id].add("spambotAdderProfile")

    if any(indicator in user__full_name for indicator in only_drug_indicators):
        tags_detected[user__id].add("drugseller")

        non_drugseller_tags = {"datingOrPornProfile", "spambotAdderProfile"}
        if tags_detected[user__id] & non_drugseller_tags:
            for tag in non_drugseller_tags:
                try:
                    tags_detected[user__id].remove(tag)
                except KeyError:
                    pass

    if not tags_detected[user__id]:
        #  (629) 245-2157
        if not any(c.isalpha() for c in user__first_name):
            tmp_counter = 0
            if user__first_name.startswith("(") and ")" in user__first_name and "-" in user__first_name:
                for c in user__first_name:
                    if c.isdigit():
                        tmp_counter += 1
                if tmp_counter > 8:
                    tags_detected[user__id].add("namingscheme")
        elif user__first_name.count(" ") in [1, 2, 3] and "last_name" not in user_dict:
            tags_detected[user__id].add("fullnamefirst")

    return tags_detected


def check_text_for_ban_reasons(text: str) -> list:
    tags_detected = []

    if any(indicator in text.lower() for indicator in inheritance_indicators):
        tags_detected = ["inheritanceScam"]

    if any(indicator in text.lower() for indicator in drugspam_indicators):
        tags_detected = ["drugseller"]

    if not tags_detected:
        if any(indicator in text.lower() for indicator in investment_trading_indicators):
            tags_detected = ["investmentTrading"]

    return tags_detected


def check_username_for_dopp(username: str) -> bool:
    cropped_username, _ = remove_duplicate_letters_and_trailing_number(username)

    if any(indicator == cropped_username.lower() for indicator in list_of_dopp_full_indicators) or \
       any(indicator in cropped_username.lower() for indicator in list_of_dopp_part_indicators) or \
       any(cropped_username.lower().startswith(indicator) for indicator in list_of_dopp_part_starters):
        return True
    return False


def remove_duplicate_letters_and_trailing_number(text: str) -> (str, bool):
    no_duplicate_letter = text[0]
    ends_with_number = False
    for char in text:
        if no_duplicate_letter[-1] != char:
            no_duplicate_letter = f"{no_duplicate_letter}{char}"

    no_duplicate_letter_no_trailing_number = no_duplicate_letter
    for _i in range(len(no_duplicate_letter_no_trailing_number)):
        if no_duplicate_letter_no_trailing_number[-1] in ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9"):
            no_duplicate_letter_no_trailing_number = f"{no_duplicate_letter_no_trailing_number[:-1]}"
            ends_with_number = True
    return no_duplicate_letter_no_trailing_number, ends_with_number
