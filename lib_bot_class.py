# coding=utf-8
"""Implementing interaction with TG web API and readout of local config file"""
from __future__ import annotations
from typing import Optional

import configparser as cfgp
import json
import urllib.parse
import requests
from time import time, sleep

try:
    from lib_bot_logging import (log_to_file, unknown_response)
    import lib_spam_federation_helper_bot_textstrings as texts
except ModuleNotFoundError:
    print("\n\n ! lib_bot_class.py"
          "\n This code should only be running in the unit test files.   !\n\n")


def message_to_send_is_effectively_empty(msg: str) -> bool:
    if msg == texts.message_from_bot__title:
        return True
    if msg.startswith(texts.warning_id_of_fedworker):
        if "✅" in msg or "⛔️" in msg or "🚥" in msg:
            return False
        else:
            return True
    return False


def INIT_create_API_BASE_URL(cfg: str) -> [str, str]:
    tmp_file = read_cfg(cfg, "cred", "tk_file")
    token = read_cfg(tmp_file, "cred", "tk")
    return f"https://api.telegram.org/bot{token}/", token


class TelegramBot:
    """Bot class for receiving messages and sending replies.
    Generally, all replies are going to where "questions" came from, except:
        * old pending messages                      (delay_detected)
        * duplicate messages                        (last_message_mainchat, DUPLICATES__CHAT_ID)
    thus, its implementation can be confusing :)
    """
    timestamps_per_chat: dict[int, list]

    def __init__(self, cfg):
        self.timestamps_per_chat = dict()
        self.updates = None
        self.base_url, self.token = INIT_create_API_BASE_URL(cfg)
        self.debug = (read_cfg(cfg, "settings", "debug").lower()
                      in ["true", "yes", "on"])  # refactored if-statement
        self.BOT_OWNER_ID = int(read_cfg(cfg, "bezugsgruppe", "bot_owner_id"))
        self.BOT_ID = int(self.token.split(":")[0])
        self.BOT_USERNAME = read_cfg(cfg, "cred", "username_of_bot")  # kann leer sein
        self.MAINCHAT_ID = int(read_cfg(cfg, "bezugsgruppe", "write_to_chat_id"))
        self.MAINCHAT_LOG_ID = int(read_cfg(cfg, "bezugsgruppe", "logging_rose_ban_messages_chat_id"))

        # Test-Mode schreibt mehr Service-Nachrichten nach MAINCHAT_ID
        # betrifft nicht die DUPLICATES
        self.TEST_MODE = read_cfg(cfg, "bezugsgruppe", "test_mode", is_boolean=True)
        self.DUPLICATES_CHAT_ID = int(read_cfg(cfg, "bezugsgruppe", "write_duplicates_to_chat_id"))
        if self.DUPLICATES_CHAT_ID == -1:
            self.DONT_SEND_DUPLICATES = True
            self.DUPLICATES_CHAT_ID = self.BOT_OWNER_ID  # wird für is_message_deleted() benötigt
        else:
            self.DONT_SEND_DUPLICATES = False
        self.delay_detected_and_reported = False

        self.use_this_ban_reason_in_every_create_fban_message = read_cfg(cfg, "bezugsgruppe", "default_ban_reason")
        # weil der Bot nach Crash beim Start auch veraltete Updates bekommen kann, erstmal True:
        self.SEND_MESSAGES_TO_DUPLICATES_CHAT = True

        self.update_id = 0
        self.last_message_ID = 0
        self.last_message_mainchat = ""
        self.LOG_MSGS = read_cfg(cfg, "log", "message_dump")
        self.LOG_IGNR = read_cfg(cfg, "log", "ignore_message_dump")
        self.MAIN_LOG = read_cfg(cfg, "log", "log_file")
        if not self.LOG_MSGS or not self.LOG_IGNR or not self.MAIN_LOG:
            raise TypeError("Namen unter denen die Logfiles gespeichert werden, sollten nicht leer sein!")
        self.AUTO_WHITELISTING_NEW_USERS = True

        if self.debug:
            self.send_message_to_owner(f"{texts.debug__prefix}{texts.debug__bot_started}\n"
                                       f"MAINCHAT_ID: {self.MAINCHAT_ID}\n"
                                       f"MAINCHAT_LOG_ID: {self.MAINCHAT_LOG_ID}\n"
                                       f"TEST_MODE: {self.TEST_MODE}\n"
                                       f"DONT_SEND_DUPLICATES: {self.DONT_SEND_DUPLICATES}\n"
                                       f"DUPLICATES_CHAT_ID: {self.DUPLICATES_CHAT_ID}\n")

    def change_mainchat_id(self, migrated_to_this_chat_id: int) -> None:
        if isinstance(migrated_to_this_chat_id, int) and str(migrated_to_this_chat_id).startswith("-100"):
            self.MAINCHAT_ID = migrated_to_this_chat_id
        else:
            raise ValueError("change_mainchat_id should receive integer value and it should start with -100")

    def get_updates(self, timeout: int = 60) -> None:
        """timeout-Parameter sagt, wie lange beim Polling gewartet wird
        größerer Wert → dauert länger bis auf SIGINT reagiert wird"""
        self.updates = None  # Das bewirkt Garbage Collection, nice
        url = f"{self.base_url}getUpdates?timeout={timeout}"
        # ignoriert editierte Nachrichten: siehe https://core.telegram.org/bots/api#update
        url += '&allowed_updates=["message", "channel_post", "my_chat_member", "callback_query"]'
        if self.update_id:
            url += f"&offset={self.update_id + 1}"
        try:
            r = requests.get(url)
        except Exception as e:
            log_to_file("requests.get(...) failed!", self.MAIN_LOG, verbose=True)
            log_to_file(str(e), self.MAIN_LOG, verbose=True)
        else:
            try:
                self.updates = json.loads(r.content)
            except json.JSONDecodeError as e:
                error_message = "failed command: json.loads(getUpdates-request))"
                print(error_message, e)
                log_to_file(error_message, self.MAIN_LOG, verbose=True)

    def get_updates_failed(self) -> [bool, int]:
        """Überprüft das Update-JSON, schreibt bei Fehler ins Log.
        Gibt Bool und Wartezeit zurück"""
        if not self.updates:
            log_to_file("No answer from TG server!", self.MAIN_LOG, verbose=True)
            return True, 15
        if "ok" not in self.updates:
            unknown_response(self.updates, "ok", self.MAIN_LOG)
            return True, 15
        if not self.updates["ok"]:
            # log_to_file("Server error!", self.MAIN_LOG, verbose=True)
            log_to_file(str(self.updates), self.MAIN_LOG, verbose=True)
            if "error_code" in self.updates:
                if self.updates["error_code"] == "429":
                    return True, self.updates["parameter"]["retry_after"] + 5
                elif self.updates["error_code"] == "502":
                    return True, 30
                elif self.updates["error_code"] == "504":
                    return True, 60
            return True, 15
        if "result" not in self.updates:
            unknown_response(self.updates, "result", self.MAIN_LOG)
            return True, 15
        return False, 0

    def send_message(self, msg: str, chat_id: int, reply_to_msg_id: int = 0, markdown: int | bool = 0) -> dict:
        """
        1. Non-informative messages are not sent - see message_to_send_is_effectively_empty()
        2. Nur an MAINCHAT_ID: Vergleiche aktuelle mit letzter versandten Nachricht, falls identisch: umlenken
        3. If trying to send empty messages, log this.
        """
        if message_to_send_is_effectively_empty(msg):
            return {}

        if chat_id == self.MAINCHAT_ID:
            # Identische Meldungen im MAINCHAT reduzieren
            if msg == self.last_message_mainchat and msg:
                self.SEND_MESSAGES_TO_DUPLICATES_CHAT = True
            elif not self.SEND_MESSAGES_TO_DUPLICATES_CHAT:
                self.last_message_mainchat = msg

        if self.DONT_SEND_DUPLICATES and self.SEND_MESSAGES_TO_DUPLICATES_CHAT:
            # ungewöhnliches Verhalten (hier: keine Reaktion) soll sichtbar sein, print-command ist günstig
            print(texts.console__not_sending_duplicates_or_old_replies)
            return {}

        if self.SEND_MESSAGES_TO_DUPLICATES_CHAT:
            tmp_chat_id = self.DUPLICATES_CHAT_ID
        else:
            tmp_chat_id = chat_id

        if self.SEND_MESSAGES_TO_DUPLICATES_CHAT:
            if chat_id != tmp_chat_id:
                # 'Antwort-auf' ergibt nur im gleichen Chat Sinn
                reply_to_msg_id = 0

        if not msg:
            warning_logmsg = f"Tried to send empty message to {chat_id} responding to message_id {reply_to_msg_id}"
            log_to_file(warning_logmsg, self.MAIN_LOG, verbose=True)
            if self.debug:
                self.send_message_to_owner(f"{texts.debug__prefix}{warning_logmsg}")
            return {}
        msg__percent_encoded_special_URI_characters = urllib.parse.quote(msg, safe="")
        url = f"{self.base_url}sendMessage?disable_web_page_preview=true&disable_notification=true&"\
            "chat_id={tmp_chat_id}"
        url = f"{url}&text={msg__percent_encoded_special_URI_characters}"

        if markdown:
            url += "&parse_mode=MarkDown"
            if markdown == 2:
                url += "V2"
        if reply_to_msg_id:
            url += f"&reply_to_message_id={reply_to_msg_id}&allow_sending_without_reply=true"

        self.keep_time_limit__using_sleep(tmp_chat_id)
        response_TG_json: dict = json.loads(requests.get(url).content)
        if "error_code" in response_TG_json:
            print(f"\n\t{texts.sendmessage__error}")
            print(msg)
        else:
            try:
                if response_TG_json["result"]["chat"]["id"] == self.MAINCHAT_ID:
                    self.last_message_ID = response_TG_json["result"]["message_id"]
            except KeyError as e:
                error_message = texts.sendmessage__broken_json
                log_to_file(error_message, self.MAIN_LOG, verbose=True)
                print(error_message, "\n\n", e)
                self.last_message_ID = 0
        try:
            print(response_TG_json["result"]["text"])
        except KeyError:
            print(response_TG_json)
        return response_TG_json

    def send_message_to_owner(self, msg: str, reply_to_msg_id: int = 0, markdown: int | bool = 0) -> dict:
        """Just a wrapper for send_message"""
        if self.TEST_MODE:
            recipient_id = self.MAINCHAT_ID
        else:
            recipient_id = self.BOT_OWNER_ID
        return self.send_message(msg, recipient_id, reply_to_msg_id, markdown)

    def leave_chat(self, chat_id: int, username: str = "", title: str = "") -> object:
        url = self.base_url + "leaveChat?chat_id=" + str(chat_id)
        if self.debug:
            print(f"{texts.debug__prefix}{texts.debug__console__leaving_chat}")
        notification = f"Verlasse {title} [Chat]"
        if username:
            notification += f" @{username}"
        else:
            notification += f"(tg://user?id={chat_id}) '{chat_id}'"
        self.send_message_to_owner(notification, markdown=True)
        success = requests.get(url)
        return success

    def send_deletable_message(self, msg: str, button_text: str,
                               chat_id: int, reply_to_msg_id: int = 0, markdown: int = 0) -> dict:
        # Werden nicht darauf überprüft, ob sie Duplicates sind

        if not button_text:  # not as default value, so order can be kept; empty buttons won't be displayed
            button_text = "<Keine Text für Löschbutton angegeben>"
        button = """reply_markup={%22inline_keyboard%22:[[{%22text%22:%22""" + button_text + \
                 """%22,%22callback_data%22:%22Delete%22}]]}"""
        msg__percent_encoded_special_URI_characters = urllib.parse.quote(msg, safe="")

        send_url = f"{self.base_url}sendMessage?disable_web_page_preview=true&disable_notification=true&" \
                   f"chat_id={chat_id}&text={msg__percent_encoded_special_URI_characters}&{button}"
        if reply_to_msg_id:
            send_url += f"&reply_to_message_id={reply_to_msg_id}"

        if markdown == 0:
            pass
        elif markdown == 1:
            send_url += "&parse_mode=Markdown"
        elif markdown == 2:
            send_url += "&parse_mode=MarkdownV2"

        self.keep_time_limit__using_sleep(chat_id)
        response_TG = requests.get(send_url)
        return json.loads(response_TG.content)

    def is_message_deleted(self, message_id: int) -> Optional[bool]:
        # Funktioniert nur mit Nachrichten aus MAINCHAT
        url_to_test_forwarding = f"{self.base_url}forwardMessage?chat_id={self.DUPLICATES_CHAT_ID}&from_chat_id=" \
                                 f"{self.MAINCHAT_ID}&disable_notification=true&protect_content=true&" \
                                 f"message_id={message_id}"
        response_TG = requests.get(url_to_test_forwarding)
        try:
            response_TG_json = json.loads(response_TG.content)
        except json.JSONDecodeError as e:
            print(texts.after_URL_calls__JSONDecodeError, e)
        else:
            if "error_code" in response_TG_json:
                return True
            else:
                # Forward hat geklappt, sollte aber nicht weiter belästigen
                if "ok" in response_TG_json:
                    if response_TG_json["ok"]:
                        self.delete_message(response_TG_json["result"]["message_id"],
                                            response_TG_json["result"]["chat"]["id"])
                        return False
        return None

    def delete_message(self, message_id: int, chat_id: int) -> None:
        url_to_delete_a_msg = f"{self.base_url}deleteMessage?message_id={message_id}&chat_id={chat_id}"
        response_tg = requests.get(url_to_delete_a_msg)  # tg.org/api: "Returns True on success."
        try:
            response_tg_json = json.loads(response_tg.content)
        except json.JSONDecodeError as e:
            print(texts.after_URL_calls__JSONDecodeError, e)
        else:
            if "error_code" in response_tg_json:
                print(texts.console__deleting_msg_failed)

    def answerCallbackQuery(self, callback_query_id: int, text: str = "") -> None:
        url_to_answer_callback_query = f"{self.base_url}answerCallbackQuery?callback_query_id={callback_query_id}&" \
                                       "text={text}"
        sucess_bool_or_error_code = requests.get(url_to_answer_callback_query)
        if self.debug:
            try:
                response_to_answer_json = json.loads(sucess_bool_or_error_code.content)
            except json.JSONDecodeError as e:
                print(texts.after_URL_calls__JSONDecodeError, e)
            else:
                if "error_code" in response_to_answer_json:
                    print(texts.debug__answerCallbackQuery_failed)
                    self.send_message_to_owner(f"{texts.debug__prefix}{texts.debug__answerCallbackQuery_failed}")
                    log_to_file(texts.debug__answerCallbackQuery_failed, self.MAIN_LOG, verbose=True)
                    print(response_to_answer_json)
                    log_to_file(str(response_to_answer_json), self.MAIN_LOG, verbose=True)

    def keep_time_limit__using_sleep(self, chat_id: int) -> None:
        """
        Avoiding floodwait limit by sleeping per Chat. Should be called by send_message and alike.
        It is ignoring sleeping periods related to self.get_updates(), as this shall be seperated questions.
        On purpose not freezing time by reusing the return value of time(). Precision below 0.1 seconds is irrelevant.
        A)      Check parameter
        B)      Private chats are handled seperately and not optimized. -> return
        C)      create list if chat_id is new
        D) return if list is empty (always after 3)
        E) return if list has only timestamps which are older than 60s
        F) 1st limit: 1 msg / sec
        G) 2nd limit: 20 msgs / min
        """

        if not isinstance(chat_id, int):
            raise TypeError("limit_handler should get an int.")
        if not chat_id:
            raise ValueError("limit_handler should be called with an chat_id")
        if 0 < chat_id <= 99999:
            raise ValueError("limit_handler got a unusual low userID as chat_id. Should not be trying to send there.")

        if chat_id > 99999:
            #  core.telegram.org/bots/faq#my-bot-is-hitting-limits-how-do-i-avoid-this
            #  "the API will not allow more than 30 messages per second or so."
            # Privatchat
            sleep(1 / 30)
            return

        if chat_id not in self.timestamps_per_chat:
            self.timestamps_per_chat.update({chat_id: list()})
        if len(self.timestamps_per_chat[chat_id]) == 0:
            self.timestamps_per_chat[chat_id].append(time())
            return

        min_interval = time() - max(self.timestamps_per_chat[chat_id])

        if min_interval >= 60:
            single_new_timestamp = list()
            single_new_timestamp.append(time())
            self.timestamps_per_chat[chat_id] = single_new_timestamp
            return

        # F.1) ignore for up to 4 messages
        if len(self.timestamps_per_chat[chat_id]) < 4:
            # Burstmode; "avoid sending more than 1 message per second.
            # We may allow short bursts that go over this limit"
            pass
        # F.2) wait so that 1 sec interval is reached
        elif min_interval < 1.06:
            sleep(1.06 - min_interval)

        # G.1) -> get all within last min
        cleaned_up_timestamps = list()
        for timestamp in self.timestamps_per_chat[chat_id]:
            if time() - timestamp < 60:
                cleaned_up_timestamps.append(timestamp)

        # G.2) -> 20 msgs? then sleep until the oldest timestamp is older than 60 secs
        if len(cleaned_up_timestamps) >= 20:
            minimum_sleep_time = 60 - (time() - min(cleaned_up_timestamps))
            try:
                sleep(minimum_sleep_time)
            except ValueError:
                pass  # um negative Werte abzufangen
            else:     # falls gewartet wurde -> erneuter Cleanup
                max_interval = time() - min(cleaned_up_timestamps)
                if max_interval > 60:
                    cleaned_up_timestamps = list()
                    for timestamp in self.timestamps_per_chat[chat_id]:
                        if time() - timestamp < 60:
                            cleaned_up_timestamps.append(timestamp)
        else:
            cleaned_up_timestamps.append(time())

        self.timestamps_per_chat[chat_id] = cleaned_up_timestamps
        return


def read_cfg(config, section, key, is_boolean=False):
    """Liest Werte aus der [config] aus.
     Schreibt nicht ins Log, falls die Config unvollständig, kaputt oder nicht vorhanden ist. Denn Logging wird über
     diese Konfigurationsdatei erst ermöglicht.

     Leere Angaben bei Strings und Int werden unterschiedlich behandelt.
        Int = "" -> raise Exception
        Str = "" -> return ""
    Alternativ wäre parser = cfgp.ConfigParser(allow_no_value=True) möglich. Dann müssten die int = 0 entsprechend
    abgefangen werden -> raise KeyError(warning_config_file_broken_or_not_prepared)
    """
    parser = cfgp.ConfigParser()
    parser.read(config)
    try:
        if is_boolean:
            return parser.getboolean(section, key)
        return parser.get(section, key)
    except cfgp.NoSectionError:
        warning_config_file_broken_or_not_prepared = f"{config}: Abschnitt [{section}] " \
                                                     "nicht vorhanden oder Datei falsch benannt."
        raise KeyError(warning_config_file_broken_or_not_prepared)
    except cfgp.NoOptionError:
        warning_config_file_broken_or_not_prepared = f"{config}: Wert für {key} in [{section}] nicht vorhanden."
        print(warning_config_file_broken_or_not_prepared)
        if key.endswith("_id"):
            # unerwartetes Verhalten, wenn keine (korrekten) Zahlen eingetragen sind -> lieber abbrechen
            raise KeyError(warning_config_file_broken_or_not_prepared)
        else:
            return ""
