# -*- coding: utf-8 -*-
from datetime import datetime


def log_to_file(note: str, log_file: str, verbose=False):
    """Easy print to given log file with UTC time, optional print to console"""
    try:
        with open(log_file, "a") as log:
            print(datetime.utcnow().__str__()[:16], " ", note, file=log)
            if verbose:
                print(datetime.utcnow().__str__()[:16], " ", note, "\n")
    except OSError:
        print("Konnte Log-Eintrag nicht in Datei schreiben:\n")
        print(note)
        print("\n\n")


def unknown_response(response, key_value, log_file, logging=True):
    """Logging / reporting unusual json response of TG bot api"""
    if logging:
        log_to_file("No key >"+key_value+"< in response!",
                    log_file, verbose=True)
        log_to_file(str(response), log_file)
    else:
        print("No key >", key_value, "< in response!\n")
        print(response)


def log_message(log_file: str, json: dict):
    """Facilitating function for readability, using log_to_file"""
    log_to_file(str(json), log_file, verbose=False)


def ignore_message(log_file: str, json: dict, reason: str):
    """Facilitating function for readability, using log_to_file"""
    log_to_file(reason + " " + str(json), log_file, verbose=False)
