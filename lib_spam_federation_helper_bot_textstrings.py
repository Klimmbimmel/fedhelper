# -*- coding: utf-8 -*-
try:
    from hidden_variables import link_angeheftete_hilfe_kanalweiterleitung
except ModuleNotFoundError:
    print("\n\n ! lib_spam_federation_helper_bot_textstrings.py"
          "\n This part of the code should only be running in the unit test files.   !\n\n")
    from fedhelper_main.hidden_variables import link_angeheftete_hilfe_kanalweiterleitung

forward_from_chat = f"Eine [Kanal-Weiterleitung enthält keine User-ID]({link_angeheftete_hilfe_kanalweiterleitung})."

# forward_from_chat_details contains markdown-V2 formattings
forward_from_chat_details =\
    "Die von mir kommentierte Nachricht ist eine Weiterleitung aus einem Kanal. Sie " \
    "enthält keine auslesbare ID des Nutzers, nur ID und Namen des Kanals." \
    "\nRose kann damit nicht spezifisch blocken. Folgende Alternativen:" \
    "\n1. Wenn du im ursprünglichen Chat auf die Spamnachricht mit `/id` antwortest, erhältst du die ID (sofern" \
    "entsprechende Bots in dem Chat sind)." \
    "\n2. Wenn du im ursprünglichen Chat eine Nachricht schreibst, in der du den Account verlinkst (tippe @ und den" \
    "Namen und wähle den korrekten(!!) Account), kannst du diese Nachricht hierher weiterleiten. Der Fedhelper-Bot" \
    "kann die ID evtl. aus der Nachricht auslesen." \
    "\n3. Die dritte Variante nutzt die Webversion oder alternative Clients von Telegram.\n" \
    "\nUnter Umständen lohnt es sich, in deiner Gruppe Weiterleitungen aus Kanälen (oder join-links) über die "\
    "Einstellungen von @MissRose_Bot bzw. @grouphelp2bot allgemein zu verbieten." \
    "\nDas funktioniert unabhängig von der Föderation, kann aber auch gut gemeinte Nutzer oder Links treffen." \
    "\n\n Manche Log-Kanäle liefern IDs und Nutzernamen von Accounts, also potentiellen Spammern. Ist der Log-Kanal " \
    "durch @MissRose_Bot gefüttert, funktioniert das Weiterleiten. Ansonsten muss der Kanal erst freigeschaltet " \
    "werden.\n Dafür bitte eine kurze Mitteilung ans Team." \
    "\n\n Anonymes Posten mithilfe eines Kanals kommt aktuell noch selten vor. Um das zu verhindern, kann mit " \
    "`/fban -100... (Channel-ID)` Rose die Nachrichten von einem Kanal blockieren."

response_bistduonline = "Ja, ich bin da. \n(Aktuell bewirken meine Einstellungen, dass ich identische Nachrichten" \
                        " nicht 2x hintereinander sende.)"
confirm_sigint = "SIGINT oder CTRL-C empfangen. Bot wird heruntergefahren."

# Ohne die Anzahl der Bans, muss bei Verwendung hinzugefügt werden
backup_confirmation = """Backup der Ban-Liste gespeichert.\nAnzahl der Bans im Backup: """
zero_backup_confirmation = """Ban-Liste ist leer. Daher wurde kein Backup geschrieben."""
# Zeilenumbruch nötig, weil der Text mit der Anzahl ergänzt wird
backup_of_preventives_confirmation = """\nSchreibe Backup der Präventiv-Ban-Liste.\nAnzahl der Bans im Backup: """
zero_backup_of_preventives_confirmation = """\nPräventiv-Ban-Liste ist leer. Daher wurde kein Backup geschrieben."""

# MAINCHAT-Mitteilungen
message_from_bot__title = "🤖 Von Bot.\n"

user_of_this_group_got_banned = "So eben wurde ein*e Nutzer*in aus dem Team gefbannt."

user_disabled_forward_ID_username_in_this_group = "\n *Nutzername* kommt auch im Team vor."
user_disabled_forward_ID = "\n User hat ID bei Weiterleitungen ausgeblendet."
user_disabled_forward_ID_but_mentioned_user = " Aber Nachrichtentext enthält IDs:\n"
id_is_from_channel_no_fban = "Die ID in der Nachricht ist von einem Kanal."

check_id_in_text__help = "Bitte immer auf die zu analysierende Nachricht antworten. Funktioniert aktuell nur: " \
                         "geteilter Kontakt, bearbeitete Caption, erneutes durchsuchen nach Mentions " \
                         "(sollte es mal nicht geklappt haben)."
check_id_in_text__no_id_found = "Konnte keine ID finden."

user_id_not_banned = "⛔️Nicht gebannt. "
user_id_is_banned = "✅ Schon gebannt."
already_fbanned_ids_are_not_commented = "Bereits gebannte IDs werden nicht gelistet."
user_id_is_banned_preventive = "🚥Präventiv gebannt."
forwarded_user_got_deleted = "\n☝️Gelöschtes Konto." \
                             " (Oder nennt sich _Deleted Account_ und es ist keine ID auslesbar.)"
mentioned_user_got_deleted = "\n🧐Verlinkung zeigt auf gelöschtes Konto." \
                             " (Vorname ist leer.) ID: "
parsed_or_mentioned_user_got_deleted = "\n🧐Verlinkung oder ausgelesene ID zeigt auf gelöschtes Konto:" \
                                       " (Vorname ist leer.) ID: "
rose_reported_user_got_deleted = "\n🧐Der gemeldete Nutzer wurde gelöscht. (Vorname ist leer.)"
rose_reported_user_is_not_linked__no_newline = "Gemeldeter Nutzer ist nicht verlinkt."
# Magic flag triggered texts:
warning_added_user_is_not_a_bot = "\nAchtung beim Abarbeiten (notfalls nachfragen, ob User oder Added by oder beide" \
                                  " gemeldet wurden.)\n"
please_note_added_user_is_a_bot = "\nHinzugefügt: Bot 👽.\n"

# BOTOWNER-Mitteilungen
private_chat_started = "\nEin privater Chat wurde gestartet, mit Nutzer: "
bot_was_added_to_group = "Bot wurde in Chat hinzugefügt, von Nutzer: "
bot_was_added_to_new_group = "Bot wurde in neu angelegten Chat hinzugefügt, von Nutzer: "

# Bot-Messages unverständlich
no_user_found = "Konnte keine ID extrahieren"

# Warnungen (Header)
# Log-Channel-Eintrag enthält Ban von Fedworker
warning_fedworker_has_been_banned = "❗️ ️User-ID wurde eben gebannt und ist von (evtl. ehemalig.)" \
                                    " Helfer:in dieser Föderation ❗"
# Rose-/id-Antwort gibt ID von Fedworker
# oder: mehrere IDs (durch Verlinkungen aktuell, evtl. in Zukunft auf bei Multi-ID-Captions)
warning_id_of_fedworker = "❗️User-ID von (evtl. ehemalig.) Helfer:in dieser Föderation!"

# unklare Meldung, Bitte um Vorsicht bei fban
warning_reporting_user_and_their_mention = "❗️Achtung, die Nachricht enthält sowohl Absender-ID als auch Verlinkung. " \
                                           "Achte bitte sehr genau darauf, welchen Account du bannst!\n"
warning_could_be_non_spammer = "❗️Achtung, die Weiterleitung könnte von einer Person sein, die Spam meldet!"

warning_using_magic_mention = {'offset': 0, 'length': 1, 'type': 'text_mention',
                               'user': {'id': 12345, 'is_bot': False,
                                        'first_name': 'Die Nachricht wurde von mir nicht verstanden'}}
warning_low_id_identified_by_starting_digit = "❗️Nicht verwechseln, ID ist keine neue (auch wenn sie mit 2, 5, 6 oder 7 anfängt)."
# deletable Messages
all_IDs_in_caption_have_been_banned = "Alle IDs bereits gebannt. (Bei Nachreichung diesen Hinweis gern löschen.)"

# Kommandos
# ( nicht vollständig, da /info und /fban@<botusername> auch beantwortet werden )
command__all_users__ping = "/bistduonline"
command__all_users__help_forwarded_message = "/hilfe_weitergeleitete_nachricht"
command__add_someone_to_fedworkerslist = "/add2fedworkerslist"
command__reload_changes_of_file_with_all_fedworkers = "/rereadworkerlist"
command__do_backup_of_banlist = "/savecurrentbanlist"
command__check_if_id_on_banlist = "/isoncurrentbanlist"
command_reload_libs = "/testnewfeature"
command__check_msg_for_id = "/pruefe_id_aus_text"

old_command_add = "/add2banlist"
old_command_remove = "/removefrombanlist"
# Kommandos - Nachrichten an Owner
add2banlist_failed = "Nummer nicht korrekt (ValueError), bitte überprüfen"
remove_from_banlist_failed = add2banlist_failed
add2banlist_help = "Should not be used any more! Thus removed!"
remove_from_banlist_help = "Should not be used any more! Thus removed!"
isoncurrentbanlist_help = f"`{command__check_if_id_on_banlist}` <user-id>\n{command__check_if_id_on_banlist} <user-id>"

old_add2banlist_help = f"\n`{old_command_add}` <user-id>\n{old_command_add} <user-id>"
old_remove_from_banlist_help = f"`{old_command_remove}` <user-id>\n{old_command_remove} <user-id>"

warning_mainchat_id_has_been_changed = " hat ID des Mainchat geändert.\nTemporär gewhitelistet.\n" \
                                       "❗ Unbedingt in den Settings einfügen ❗"  # Startet mit UID
warning_unknown_exception = "Generelle Exception ausgelöst, go Sherlock!"
warning_delay_detected = "Eingehende Updates sind älter als "
warning_delay_detected2 = " Sekunden. Sie werden ignoriert um die Abarbeitung nicht zu verlangsamen."

warning_early_KeyboardInterrupt = "SIGINT caught, bitte Backup-Dateien von Bans und Prev-Bans kontrollieren."

# errors to raise
exc_no_ID__rose_log_check_and_get_id = "rose_log_check_and_get_id: konnte ID nicht verstehen. Sollte nicht passieren." \
                                       " Wurde der Standardtext angepasst?"

# Optionale Warnungen (if debug=True)
warning_broken_message__no_message_id = "The incoming message didn't have a message_id. This might be a problem." \
                                        " (But maybe it's just an incoming CallbackQuery or something else special.)"

# Nachrichten in Console
console__backup_failed = "Konnte kein Backup schreiben."

console__please_wait_polling_timeout = "Das Skript wird erst nach Ablauf des Pollings (getUpdates-Timeout) beendet. " \
                                       "Bitte einen Moment Geduld.\n (cd <dir_of_bot> && git pull) in der Zwischenzeit."
console__restart_within_24hours = "Bitte innerhalb von 24h neustarten, sonst ist die Banlist out of sync."
console__sigint_notification = " SIGINT caught"
console__sigint_finished = "Exit after SIGINT:"
console__read_again_log_entry = "Lese alten Logchannel-Eintrag erneut\nAchtung, bitte spätere Fed-Unbans beachten!!"
console__deleting_msg_failed = "Konnte Nachricht nicht löschen."
console__no_backup_file_of_bans_reading_export = "Keine Backup-Datei gefunden, lese Export-Datei ein"
console__no_export_file_found = "Keine Export-Datei gefunden."
console__no_preventives_backup_file_of_bans = "Keine Backup-Datei für präventive Bans gefunden."
console__backup_found = "Lese Backup-Datei ein."
console__broken_line_in_backup_file = "Fehlerhafte Zeile in Backup!"
console__broken_line_in_export_file = "Fehlerhafte Zeile in Export!"
console__preventives_backup_found = "Lese Backup-Datei für präventive Bans ein."
console__verbose_init_finished = "Initialisierung abgeschlossen"
console__verbose_not_able_to_remove_user_id_from_BAN_SET = "Zu entfernende ID nicht gefunden."

console__very_unlikely__filename_for_renaming_backup_used_by_dir = "Umbenennung des alten Backups fehlgeschlagen, weil" \
                                                                   " unter dem Ziel-Namen ein Verzeichnis existiert."
console__very_unlikely__filename_for_renaming_backup_used_by_file = "Umbenennung des alten Backups fehlgeschlagen, weil" \
                                                                    " unter dem Ziel-Namen eine Datei existiert." \
                                                                    " Versuche zu überschreiben."
console__very_very_unlikely__OSError_after_FileExistsError = "Überschreiben fehlgeschlagen."

console__readout_df_file__file_broken = "Error ahead. Dictionary file corrupted:"
console__readout_df_file__line_broken = "Fehlerhafte Zeile in"
console__readout_df_file__double_line = "ID doppelt vorhanden in"

console__not_sending_duplicates_or_old_replies = "Not sending old or duplicated replies," \
                                                 " due to setting of self.DONT_SEND_DUPLICATES."

log__unknown_expection = "!unbekannte Exception ausgelöst, letzte Nachricht und Exception:"
log__logchannel_renamed = "Logchannel has been renamed."
log__logchannel_unknown_msg = "Unbekannte Nachricht in Fed-Logchannel empfangen."
after_URL_calls__JSONDecodeError = "failed command: json.loads(response_TG.content))"
sendmessage__error = "  -- ERROR WHILE SENDING --"
sendmessage__broken_json = "Could not get recipient and id of message just sent."

debug__prefix = "debug: "

debug__bot_started = "The bot has been started."
debug__console__leaving_chat = "Leaving chat..."
debug__answerCallbackQuery_failed = "answerCallbackQuery lieferte Errorcode zurück."
debug__get_updates_failed = "get_updates_failed == True"
