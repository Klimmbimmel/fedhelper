# coding=utf-8
from __future__ import annotations

try:
    from lib_spam_federation_helper_bot_textstrings import warning_using_magic_mention
    from lib_spam_federation_helper_bot import dict_of_users_in_msg_entities, is_uid_mentioned_and_return_first_and_last_name
    import lib_spam_federation_helper_bot_textstrings as texts
except ModuleNotFoundError:
    print("\n\n ! parsing.py"
          "\n This part of the code should only be running in the unit test files.   !\n\n")
    from fedhelper_main.lib_spam_federation_helper_bot_textstrings import warning_using_magic_mention
    from fedhelper_main.lib_spam_federation_helper_bot import dict_of_users_in_msg_entities, is_uid_mentioned_and_return_first_and_last_name
    import fedhelper_main.lib_spam_federation_helper_bot_textstrings as texts

minimum_length_of_single_id = 7  # kürzere sind selten oder wie 777000 (Service Notifications) ein TG Service
maximum_length_of_single_id = 10

vhznet_voteban_start_str = "user vote accepted.\n["
vhznet_voteban_name_and_id_separator = "](tg://user?id="
vhznet_voteban_end_str = ") was banned."
vhznet_votebanned = [vhznet_voteban_start_str, vhznet_voteban_name_and_id_separator, vhznet_voteban_end_str]

rose_id_engl_start_str = "User "
rose_id_engl_name_and_id_separator = "'s ID is "
rose_id_ger_start_str = "Die ID des Nutzers "
rose_id_ger_name_and_id_separator = " lautet "
rose_id_end_str = "."
rose_id_engl = [rose_id_engl_start_str, rose_id_engl_name_and_id_separator, rose_id_end_str]
rose_id_ger = [rose_id_ger_start_str, rose_id_ger_name_and_id_separator, rose_id_end_str]

rose_id_engl_long = "\nThe forwarded "
rose_id_ger_long = "\nDie ID des weitergeleiteten Nutzers, "

rose_info_engl_start_str = "User info:\nID: "
rose_info_engl_name_and_id_separator = "\nFirst Name: "
rose_info_end_str = ""
rose_info_engl = [rose_info_engl_start_str, rose_info_engl_name_and_id_separator, rose_info_end_str]

rose_report_engl_start_str = "Reported "
rose_report_engl_name_and_id_seperator = " ["
rose_report_engl_end_str = "] to admins."
rose_report_engl = [rose_report_engl_start_str, rose_report_engl_name_and_id_seperator, rose_report_engl_end_str]
rose_report_ger_start_str = ""
rose_report_ger_name_and_id_seperator = " ["
rose_report_ger_end_str = "] Die Meldung an die Admins ist erfolgt."
rose_report_ger = [rose_report_ger_start_str, rose_report_ger_name_and_id_seperator, rose_report_ger_end_str]

banof_voteban_finished_marker = " has been kicked — the only way to get this user back is for admins to manualy unban in chat settings.\n\nVoters who chose to kick:"
banof_voteban_started__marker = " would like to kick "
banof_voteban_started__end_str = ". Do you agree?\nPowered by Todorant"


def banof__remove_entities_of_non_spammers(full_message: dict) -> dict:
    """Behalte alle Elemente der Message, außer von message["entities"] nur die mit passendem Offset
    beim laufendem Voteban ist der zu bannenende Account am Ende:
    "@a_user would like to kick @a_spammer. Do you agree?\nPowered by Todorant"   vs.
    "@a_spammer has been kicked..." """

    maximum_offset_of_banned_or_saved_user = 9
    cleaned_up_message = dict()
    cleaned_up_message.update({"entities": []})
    reverse_order = False

    if banof_voteban_started__marker in full_message["text"] and \
       full_message["text"].endswith(banof_voteban_started__end_str):
        reverse_order = True
    elif banof_voteban_finished_marker not in full_message["text"]:
        # andere Nachrichten werden nicht wirklich verstanden, Workaround, für unerwartete
        if "entities" not in full_message:
            full_message.update({"entities": []})
        full_message["entities"].append(warning_using_magic_mention)
        return full_message
    for item in full_message:
        if item != "entities":
            # Einfach kopieren
            cleaned_up_message.update({item: full_message[item]})
        else:
            for en_item in full_message["entities"]:
                if not reverse_order:
                    if en_item["offset"] < maximum_offset_of_banned_or_saved_user:
                        cleaned_up_message["entities"].append(en_item)
                elif en_item["offset"] > maximum_offset_of_banned_or_saved_user:
                    cleaned_up_message["entities"].append(en_item)
    return cleaned_up_message


def check_id_in_text(reply_to_message: dict) -> (int | list, dict):
    """Gibt zurück:
    1. einzelne ID in dict oder Liste basierend auf caption oder mentioned users
    2. dictionary mit den reason_tags_by_uid
    """
    if "contact" in reply_to_message:
        try:
            user_ID = reply_to_message["contact"]["user_id"]
        except KeyError:
            # Telegram sagt, die ID ist optional... vielleicht bei Teilen einer VCard einer Person, die kein TG hat?
            return {}, {}
        else:
            return {user_ID: reply_to_message["contact"]["first_name"]}, {}
    elif "caption" in reply_to_message:
        return extract_ids_or_id_and_reasons(reply_to_message["caption"])
    elif "mentions" in reply_to_message:
        # normalerweise nicht nötig, aber denkbar nach Crash / Neustart, neuen Bots und anderen Formatierungen
        return dict_of_users_in_msg_entities(reply_to_message)
    elif "text" in reply_to_message:
        return extract_ids_or_id_and_reasons(reply_to_message["text"])

    return 0, {}


def sangm_rename(msg_txt: str) -> (str, str, str, dict):
    parsed_id = ""
    parsed_first_name = ""
    parsed_username = ""
    if not msg_txt.startswith("User ") and not msg_txt[5].isnumeric():
        return parsed_id, parsed_first_name, parsed_username, {}

    parsed_id = msg_txt[len("User "):]
    parsed_id = parsed_id.split(" ")[0]
    rest_text = msg_txt[len("User ") + len(parsed_id) + 1:]
    if rest_text.startswith("changed name from"):
        parsed_first_name = rest_text[::-1].split(" ot ")[0][::-1]
    elif rest_text.startswith("changed username from "):
        parsed_username = rest_text[::-1].split(" ot ")[0][::-1]
        parsed_first_name = "Nutzername nicht auslesbar"
    return parsed_id, parsed_first_name, parsed_username, {int(parsed_id): {"renamed"}}


def readout_id_and_name_from_formatted_str(msg_txt: str, seps: list,
                                           id_and_name_reverse_order: bool = False) -> (str, str):
    """seps enthält einen Start-String, ID-und-Name-trennenden String und einen End-String"""
    extracted_id = ""
    extracted_name = ""
    if msg_txt.startswith(seps[0]) and seps[1] in msg_txt and msg_txt.endswith(seps[2]):
        tmp = msg_txt[len(seps[0]):]
        tmp = tmp.rstrip(seps[2])
        # Um zu verhindern, dass sehr spezielle Nutzernamen stören, kurz magic code.
        reverse_tmp = tmp[::-1]
        reversed_str_to_cut_out = seps[1][::-1]
        reverse_tmp = reverse_tmp.split(reversed_str_to_cut_out, maxsplit=1)
        extracted_id = reverse_tmp[0][::-1].split(".\n")[0]
        extracted_name = reverse_tmp[1][::-1]

    if id_and_name_reverse_order:
        extracted_name, extracted_id = extracted_id, extracted_name
    return extracted_id, extracted_name


def rose_response_to_command(msg_txt: str) -> (str, str):
    """deutsch: alle 3 Varianten von /id, alle 2 von /report
    englisch: alle 3 Varianten von /id, alle 2 von /report, /info (nur englisch verfügbar)"""
    extracted_id = ""
    extracted_name = ""

    # Kürze zu lange Meldungen
    if rose_id_engl_long in msg_txt:
        msg_txt = msg_txt[:msg_txt.find(rose_id_engl_long)]
    elif rose_id_ger_long in msg_txt:
        # todo: auch 2. ID testen
        msg_txt = msg_txt[:msg_txt.find(rose_id_ger_long)]
    if msg_txt.startswith(rose_info_engl_start_str):
        msg_txt = msg_txt.split("\nUser link: ")[0].split("\nUsername: ")[0].split("\nLast Name: ")[0]
        id_and_name_reverse_order = True
        extracted_id, extracted_name = readout_id_and_name_from_formatted_str(msg_txt, rose_info_engl,
                                                                              id_and_name_reverse_order)
    elif msg_txt.startswith(rose_report_engl_start_str):
        msg_txt = msg_txt.rstrip('\u200b')

        # Zur Sicherheit, falls das zusätzliche Leerzeichen irgendwann mal herausgelöscht wird
        deleted_user__breaking_alternate_version = "Reported ["
        if msg_txt.startswith(deleted_user__breaking_alternate_version):
            msg_txt = "Reported  [" + msg_txt[len(deleted_user__breaking_alternate_version):]

        if msg_txt.endswith(rose_report_engl_end_str) and "[" in msg_txt:
            # Zweiter if-Teil um Legacy-Meldungen auszufiltern
            extracted_id, extracted_name = readout_id_and_name_from_formatted_str(msg_txt, rose_report_engl)
    elif rose_report_ger_end_str in msg_txt:
        msg_txt = msg_txt.rstrip('\u200b')

        # Zur Sicherheit, falls das zusätzliche Leerzeichen irgendwann mal herausgelöscht wird
        # (oder gar nicht vorhanden ist).
        # Bei speziellen Nutzernamen wie "[1234567" ist nur fälschlicherweise ein führendes Leerzeichen.
        if msg_txt.startswith("["):
            try:
                int(msg_txt[1:minimum_length_of_single_id+1])
            except ValueError:
                pass
            else:
                msg_txt = " " + msg_txt
        if msg_txt.endswith(rose_report_ger_end_str):
            extracted_id, extracted_name = readout_id_and_name_from_formatted_str(msg_txt, rose_report_ger)
            # Siehe obiger Kommentar
            whitespace_added_when_parsing_rare_first_name = " "
            extracted_name = extracted_name.lstrip(whitespace_added_when_parsing_rare_first_name)
    elif msg_txt.startswith(rose_id_engl_start_str) and rose_id_engl_name_and_id_separator in msg_txt:
        extracted_id, extracted_name = readout_id_and_name_from_formatted_str(msg_txt, rose_id_engl)
    elif msg_txt.startswith(rose_id_ger_start_str) and rose_id_ger_name_and_id_separator in msg_txt:
        extracted_id, extracted_name = readout_id_and_name_from_formatted_str(msg_txt, rose_id_ger)

    return extracted_id, extracted_name


def rose_log_check_and_get_id(full_message: dict) -> (dict, dict):
    # Achtung sehr ähnlicher Name mit rose_log_check_and_get_id
    # User: <Vorname> ist immer vorhanden, sobald User ID vorhanden. Daher wird bei Rückgabe eines leeren Username
    # im Mainskript der Antwort-Zweig zu gelöschten Accounts gewählt.
    # Nicht alle (möglichen) Rose-Log-Nachrichten sind relevant, nur User betreffende werden geparst.

    text_to_analyze: str = full_message["text"]
    parsed_user = dict()
    id__ban_reason = dict()  # {int: set()}

    if "#WELCOME:" in text_to_analyze or "#GOODBYE:" in text_to_analyze or "#JOINREQUEST:" in text_to_analyze:
        # #WELCOME:          oder     #GOODBYE:
        # Chat: <name>
        # User: <Vorname>
        # User ID: 456
        # Private Gruppen haben noch:
        # Invitelink from: <name>    oder
        # Added by: <name> oder
        # Accepted by: <first_name> - <id>
        #   + danach
        # Invitelink from:
        #
        # Öffentliche Gruppen haben diese Zeile(n) nicht, um spezifisch zu detektieren:
        # -- bei #GOODBYE: dann
        # Message link: link
        number_of_newline_in_public_group_welcomes = 3
        if "Chat: " in text_to_analyze and "User: " in text_to_analyze and "User ID: " in text_to_analyze:
            if "Message link: " in text_to_analyze or "Invitelink from: " in text_to_analyze or\
               "Added by: " in text_to_analyze or "Accepted by" in text_to_analyze or\
               text_to_analyze.count("\n") == number_of_newline_in_public_group_welcomes:
                tmp_index = text_to_analyze.find("\n") + len("Chat: ")
                index_of_name = text_to_analyze.index("\n", tmp_index) + len("User: ") + 1
                index_end_of_name = text_to_analyze.index("\n", index_of_name)
                joining_or_leaving_fullname = text_to_analyze[index_of_name:index_end_of_name]

                index_of_id = text_to_analyze.index("User ID: ", index_end_of_name) + len("User ID: ")
                try:
                    # kein newline am Ende bei öffentlichen Gruppen
                    index_end_of_id = text_to_analyze.index("\n", index_of_id)
                except ValueError:
                    # len macht keine Unicodefehler: "Return the length (the number of items) of an object."
                    index_end_of_id = len(text_to_analyze)
                try:
                    new_or_left_user_id = int(text_to_analyze[index_of_id:index_end_of_id])
                except ValueError:
                    raise ValueError(texts.exc_no_ID__rose_log_check_and_get_id)

                sep_userid_and_adder = "Added by: "
                if sep_userid_and_adder in text_to_analyze and "entities" in full_message:
                    for item in full_message["entities"]:
                        try:
                            # ist User oder Added by verlinkt?
                            if item["user"]["id"] == new_or_left_user_id and item["user"]["is_bot"]:
                                id_of_spamBotAdder = text_to_analyze[::-1].split(" ")[0][::-1]
                                index_of_id_SPA = text_to_analyze.find(sep_userid_and_adder)
                                name_of_spamBotAdder = text_to_analyze.split(sep_userid_and_adder, index_of_id_SPA)[1][::-1].split(" - ")[1][::-1]

                                try:
                                    id__ban_reason.update({int(new_or_left_user_id): {"botAddedForSpamming"}})
                                    id__ban_reason.update({int(id_of_spamBotAdder): {"spamBotAdder"}})
                                    parsed_user.update({int(id_of_spamBotAdder): name_of_spamBotAdder})
                                except ValueError:
                                    raise ValueError(texts.exc_no_ID__rose_log_check_and_get_id)
                        except KeyError:
                            continue

                parsed_user.update({new_or_left_user_id: joining_or_leaving_fullname})

    elif "#REPORTS:" in text_to_analyze and "Chat: " in text_to_analyze and "User: " in text_to_analyze and \
         "User ID: " in text_to_analyze:
        # Reports ohne reported User ID werden mit userid=0 beantwortet
        # #REPORTS:
        # Chat: <irrelevant>
        # User: <Vorname> - irrelevant
        # User ID: 456 - irrelevant
        # Reported User: <Vorname>
        # Reported User ID: 567 <- Bingo!
        # Reported Message:
        if "Reported User: " in text_to_analyze and "Reported Message: " in text_to_analyze:
            tmp_index = text_to_analyze.find("\n") + len("Chat: ")
            tmp_index = text_to_analyze.index("\n", tmp_index) + len("User: ")
            tmp_index = text_to_analyze.index("\n", tmp_index) + len("User ID: ")
            index_of_name = text_to_analyze.index("\n", tmp_index) + len("Reported User: ") + 1
            index_end_of_name = text_to_analyze.index("\n", index_of_name)
            index_of_id = text_to_analyze.index("Reported User ID: ", index_end_of_name) + len("Reported User ID: ")
            index_end_of_id = text_to_analyze.index("\n", index_of_id)

            reported_username = text_to_analyze[index_of_name:index_end_of_name]
            try:
                reported_user_id = int(text_to_analyze[index_of_id:index_end_of_id])
            except ValueError:
                raise ValueError(texts.exc_no_ID__rose_log_check_and_get_id)
            # ID der meldenden Person wird nicht zurückgegeben
            parsed_user.update({reported_user_id: reported_username})

    elif "#BAN:" in text_to_analyze or "#MUTE:" in text_to_analyze or "#KICK:" in text_to_analyze or \
            "#WARN:" in text_to_analyze or "#WARNACTION:" in text_to_analyze or \
            "#UNBAN:" in text_to_analyze or "#UNMUTE:" in text_to_analyze:
        # #BAN: oder #MUTE: oder #KICK: or #WARN: or #WARNACTION:
        #   or #UNBAN: or #UNMUTE: <- um /funban-Vorlagen zu erzeugen bzw. zu überprüfen
        # Chat: <irrelevant>
        # Admin: <irrelevant>
        # User: <Vorname>
        # User ID: 456
        # Message link: No message link for manual actions
        # oder
        # Admin: Automated blocklist + Triggered: <trigger keyword>
        # Reason: <irrelevant>
        if "Chat: " in text_to_analyze and "Admin: " in text_to_analyze and "User: " in text_to_analyze and \
           "User ID: " in text_to_analyze and ("Message link: " in text_to_analyze or
                                               "Triggered: " in text_to_analyze or
                                               "Reason: " in text_to_analyze):
            tmp_index = text_to_analyze.find("\n") + len("Chat: ")
            tmp_index = text_to_analyze.index("\n", tmp_index) + len("Admin: ")
            index_of_name = text_to_analyze.index("\n", tmp_index) + len("User: ") + 1
            index_end_of_name = text_to_analyze.index("\n", index_of_name)
            index_of_id = text_to_analyze.index("User ID: ", index_end_of_name) + len("User ID: ")
            index_end_of_id = text_to_analyze.index("\n", index_of_id)

            sentenced_username = text_to_analyze[index_of_name:index_end_of_name]
            try:
                sentenced_user_id = int(text_to_analyze[index_of_id:index_end_of_id])
            except ValueError:
                raise ValueError(texts.exc_no_ID__rose_log_check_and_get_id)
            parsed_user.update({sentenced_user_id: sentenced_username})

    elif "#TBAN:" in text_to_analyze or "#TMUTE:" in text_to_analyze:
        # #TBAN: or #TMUTE:
        # Chat: <irrelevant>
        # Admin: <irrelevant> - nicht immer vorhanden, wenn Blocklist-Triggered
        # User: <Vorname>
        # User ID: 456
        # Message link: <irrelevant>
        # Time: <irrelevant>
        # Triggered: <irrelevant>
        # Reason: <irrelevant>
        if "Chat: " in text_to_analyze and "User: " in text_to_analyze and "User ID: " in text_to_analyze and \
           "Time: " in text_to_analyze and ("Triggered: " in text_to_analyze or "Reason: " in text_to_analyze):
            tmp_index = text_to_analyze.find("\n") + len("Chat: ")
            if "Admin: " in text_to_analyze:
                tmp_index = text_to_analyze.index("\n", tmp_index) + len("Admin: ")
            index_of_name = text_to_analyze.index("\n", tmp_index) + len("User: ") + 1
            index_end_of_name = text_to_analyze.index("\n", index_of_name)
            index_of_id = text_to_analyze.index("User ID: ", index_end_of_name) + len("User ID: ")
            index_end_of_id = text_to_analyze.index("\n", index_of_id)

            tmp_sentenced_username = text_to_analyze[index_of_name:index_end_of_name]
            try:
                tmp_sentenced_user_id = int(text_to_analyze[index_of_id:index_end_of_id])
            except ValueError:
                raise ValueError(texts.exc_no_ID__rose_log_check_and_get_id)
            parsed_user.update({tmp_sentenced_user_id: tmp_sentenced_username})

    for parsed_id in parsed_user:
        is_mentioned, first_name, last_name = is_uid_mentioned_and_return_first_and_last_name(full_message, parsed_id)
        if is_mentioned:
            mentioned_fullname = f"{first_name} {last_name}".rstrip()
            if not first_name:
                parsed_user.update({parsed_id: ""})
            elif not parsed_user[parsed_id] == mentioned_fullname:
                if parsed_id in id__ban_reason:
                    id__ban_reason[parsed_id].add("renamed")
                else:
                    id__ban_reason.update({parsed_id: {"renamed"}})
                parsed_user.update({parsed_id: mentioned_fullname})

    # TODO: evtl. später allgemeine hierarchische Ban-Reason-Detektion-Funktion nutzen (bisher ist
    # id__ban_reason.update(check_user_object_for_ban_reasons(item["user"]))

    return parsed_user, id__ban_reason


def rose_fedlog_check_and_get_id(full_message: dict) -> (bool, int, str):
    # Achtung sehr ähnlicher Name mit rose_log_check_and_get_id
    text_to_analyze: str = full_message["text"]
    fedlog_id_str = ""
    fedlog_name = ""
    is_rose_fedlog_entry = False

    list_of_seps__dt_rose_fedlog = ["Benutzer: ", "\nBenutzer-ID: ", ""]
    list_of_seps__en_rose_fedlog = ["User: ", "\nUser ID: ", ""]
    if text_to_analyze.startswith("Neuer Föderationsbann\nFöderation: "):
        magic_shortened_str = text_to_analyze.split("\nFöderationsadmin: ", maxsplit=1)[1].split("\n", maxsplit=1)[1][::-1].split("\nGrund:"[::-1], maxsplit=1)[1][::-1]
        fedlog_id_str, fedlog_name = readout_id_and_name_from_formatted_str(magic_shortened_str, list_of_seps__dt_rose_fedlog)
    elif text_to_analyze.startswith("Aktualisierung des Grundes für Föderationsbann\nFöderation: "):
        magic_shortened_str = text_to_analyze.split("\nFöderationsadmin: ", maxsplit=1)[1].split("\n", maxsplit=1)[1][::-1].split("\nVorheriger Grund:"[::-1], maxsplit=1)[1][::-1]
        fedlog_id_str, fedlog_name = readout_id_and_name_from_formatted_str(magic_shortened_str, list_of_seps__dt_rose_fedlog)

    elif text_to_analyze.startswith("New FedBan\nFed: "):
        magic_shortened_str = text_to_analyze.split("\nFedAdmin: ", maxsplit=1)[1].split("\n", maxsplit=1)[1][::-1].split("\nReason:"[::-1], maxsplit=1)[1][::-1]
        fedlog_id_str, fedlog_name = readout_id_and_name_from_formatted_str(magic_shortened_str, list_of_seps__en_rose_fedlog)
    elif text_to_analyze.startswith("FedBan Reason update\nFed: "):
        magic_shortened_str = text_to_analyze.split("\nFedAdmin: ", maxsplit=1)[1].split("\n", maxsplit=1)[1][::-1].split("\nPrevious Reason:"[::-1], maxsplit=1)[1][::-1]
        fedlog_id_str, fedlog_name = readout_id_and_name_from_formatted_str(magic_shortened_str, list_of_seps__en_rose_fedlog)

    try:
        fedlog_id = int(fedlog_id_str)
    except ValueError:
        fedlog_id = 0
    if fedlog_id or fedlog_name:
        is_rose_fedlog_entry = True

    return is_rose_fedlog_entry, fedlog_id, fedlog_name


def sophi_response_to_command(msg_txt: str) -> (str, str):
    parsed_id = ""
    parsed_first_name = ""
    if msg_txt.startswith("User info:\n"):
        pass
    elif msg_txt.startswith("Your ID: ") and "'s ID: " in msg_txt:
        name_and_id = msg_txt.split("'s ID: ", maxsplit=1)
        parsed_id = name_and_id[-1]

        # "Your ID:... " entfernen
        name_of_sophi_id_command = name_and_id[0].split("\n")
        parsed_first_name = name_of_sophi_id_command[-1]
    return parsed_id, parsed_first_name


def vhznet_rename_or_voteban_or_deleted_account(msg_txt: str) -> (str, str, str):
    parsed_id = ""
    parsed_first_name = ""
    parsed_username = ""

    marker_rename_start_str = "User <"
    marker_rename_A_general_check = "> changed names.\n\ncurrent (general check):\n"
    marker_rename_B_new_message = "> changed names.\n\ncurrent (new message):\n"
    old_marker = "> changed names.\n\n1. "
    marker_deleted_account = "removing deleted account: "
    marker_start_voteban = "Voteban in "

    if msg_txt.startswith(marker_deleted_account):
        parsed_id = msg_txt[len(marker_deleted_account):]
        return parsed_id, "", ""

    if msg_txt.startswith(marker_start_voteban):
        # 2. Zeile, Leerzeichen am Ende weg, umdrehen, ":>" (erste 2 Zeichen entfernen) und splitten
        parsed_id, parsed_first_name = msg_txt.splitlines()[1].rstrip()[::-1][2::].split("< ")
        parsed_id = parsed_id[::-1]                 # wieder zurückumdrehen
        parsed_first_name = parsed_first_name[::-1] # wieder zurückumdrehen

    if msg_txt.startswith(marker_rename_start_str) and "> changed names." in msg_txt:
        parsed_id = msg_txt[len(marker_rename_start_str):].split(">")[0]

        if marker_rename_A_general_check in msg_txt or\
                marker_rename_B_new_message in msg_txt:
            if marker_rename_A_general_check in msg_txt:
                msg_txt = msg_txt.split(marker_rename_A_general_check)[1]
            elif marker_rename_B_new_message in msg_txt:
                msg_txt = msg_txt.split(marker_rename_B_new_message)[1]

            # " | " ist Trennzeichen zw. First_name und Username
            if msg_txt.find(" | ") + 1:
                if msg_txt.find("\n") > msg_txt.find(" | "):
                    parsed_first_name = msg_txt.split(" | ")[0]
                    try:
                        parsed_username = msg_txt.split("\n")[0].split(" | @")[1]
                    except IndexError:
                        pass
                else:
                    parsed_first_name = msg_txt.split("\n")[0]
            else:
                parsed_first_name = msg_txt.split("\n")[0]
        elif old_marker in msg_txt:
            parsed_first_name = msg_txt.split("\n")[-1].split(" | ")[0]
            try:
                # " | " ist Trennzeichen zw. First_name und Username
                parsed_username = msg_txt.split("\n")[-1].split(" | ")[1]
            except IndexError:
                pass

    elif msg_txt.startswith(vhznet_voteban_start_str):
        parsed_id, parsed_first_name = readout_id_and_name_from_formatted_str(msg_txt, vhznet_votebanned)

    elif ">: [" in msg_txt.splitlines()[0]:     # id>: [reason
        msg_txt = msg_txt.splitlines()[0]
        if "] in [" in msg_txt:                 # reason] in [group
            parsed_id, parsed_first_name = msg_txt[::-1].split("[ :>")[1].split("< ")
            parsed_id = parsed_id[::-1]
            parsed_first_name = parsed_first_name[::-1]

    return parsed_id, parsed_first_name, parsed_username


def extract_ids_or_id_and_reasons(caption_as_str: str) -> (list, dict):
    """Gibt immer Liste zurück, auch wenn keine oder nur eine ID gefunden.
    Falls nur eine ID gefunden, wird nach # gesucht und dieser als Banreason interpretiert."""
    # Wenn caption vorhanden, dann von: animation, audio, document, photo, video or voice; 0-1024 chars.
    list_of_ids_in_caption = list()
    ban_reasons = list()

    if caption_as_str.startswith("/fban "):
        # /fban-Captions werden ignoriert
        return [], {}
    elif len(caption_as_str) < minimum_length_of_single_id:
        return [], {}
    elif len(caption_as_str) <= maximum_length_of_single_id:
        try:
            user_id_in_caption = int(caption_as_str)
        except ValueError:
            # TODO: Nachricht mit Lösch-Button für "/info @username",
            #  5-32 Zeichen, kein Leerzeichen, a-z, 0-9 und Unterstriche
            pass
        else:
            list_of_ids_in_caption.append(user_id_in_caption)
    elif len(caption_as_str) > maximum_length_of_single_id:
        if "\n" in caption_as_str:
            caption_as_str = caption_as_str.replace("\n", " ")
        if " " in caption_as_str:
            list_of_caption_pieces = caption_as_str.split(" ")
            for piece_of_caption in list_of_caption_pieces:
                if piece_of_caption[:1].isdigit():
                    if not (len(piece_of_caption) >= minimum_length_of_single_id):
                        continue
                    try:
                        user_id_in_caption = int(piece_of_caption)
                    except ValueError:
                        continue
                    else:
                        list_of_ids_in_caption.append(user_id_in_caption)
                elif piece_of_caption.startswith("#"):
                    #  wird nur zurückgegeben, wenn einzelne ID gefunden
                    ban_reasons.append(piece_of_caption[1:])
    if len(list_of_ids_in_caption) == 1 and ban_reasons:
        return list_of_ids_in_caption, {list_of_ids_in_caption[0]: ban_reasons}
    return list_of_ids_in_caption, {}


def unfu_merged(msg_txt: str) -> (str, str, str, dict):
    parsed_id = ""
    parsed_first_name = ""
    parsed_username = ""

    if not msg_txt.startswith("/info ") and not msg_txt.startswith("[👌]info "):
        return parsed_id, parsed_first_name, parsed_username, {}
    relevant_part_of_msg_txt = msg_txt.split("\n")[-1]

    seps = ["| ADDED BY ", " | id: ", " |"]
    username_marker = " @"
    magic_sep = "| id: "[::-1]
    id_and_username = relevant_part_of_msg_txt[::-1].split(magic_sep)[0][::-1]
    if username_marker in id_and_username:
        tmp = id_and_username[::-1].split("@ ")
        parsed_username = tmp[0][2::][::-1]
        parsed_id = tmp[1][::-1]
        _, parsed_first_name = readout_id_and_name_from_formatted_str(relevant_part_of_msg_txt, seps)
    else:
        parsed_id, parsed_first_name = readout_id_and_name_from_formatted_str(relevant_part_of_msg_txt, seps)

    ban_tag = {int(parsed_id): {"add2scamgroup"}}
    return parsed_id, parsed_first_name, parsed_username, ban_tag
