#!/usr/bin/python3.10
# -*- coding: utf-8 -*-
"""Polling code, creating bot to listen to chat and channel (via API)"""
from sys import exit as sys_exit  # refactoring exit() :shrug:
import traceback
from typing import Dict, Set    # um kompatibel mit Python 3.8 zu sein

from datetime import datetime
from signal import signal, SIGINT, SIGTERM, SIGHUP, SIGABRT, SIGKILL
from time import sleep, time
from json import dumps as json_dumps
import importlib

# import lib_spam_federation_helper_bot_textstrings as texts
from typing import Union        # um kompatibel mit Python 3.9 zu sein

import hidden_variables
import parsing
from lib_spam_federation_helper_bot import *
from interaction_with_rosebot import BanList
from hidden_variables import stop_iteration, testnewfeature
from lib_bot_logging import (log_to_file, log_message, ignore_message)
from lib_bot_class import TelegramBot
from lib_telegram import escape_markdown, from__to_readable_str, chat__to_readable_str

non_preventive_ban_reasons = ["investmentTrading", "inheritanceScam", "drugseller"]


def create_response_by_id(uid: int, name: str = "", flag_user_got_deleted: bool = False, username: str = "",
                          reasons_per_id=None) -> str:
    """Der letzte Schritt, wenn die ID gewonnen wurde, und eine Fban-Vorlage generiert werden soll oder nicht.
    Für mehr als 1 ID wird create_multi_accounts_message() genutzt.
    default_ban_reason (globale Variable) wird nur bei create_fban_message() verwendet
    reasons_per_id kann auch falsche IDs erhalten, muss nicht vorher geprüft werden
    """

    # all for debugging i think
    def serialize_sets(obj):
        if isinstance(obj, set):
            return list(obj)

        return obj

    str_reasons = json_dumps(reasons_per_id, default=serialize_sets)
    values_got = f"Got {type(uid)}: uid={uid};name={name}; flag_user_got_deleted={flag_user_got_deleted};\n" \
                 f"username={username}; reasons_per_id={str_reasons} \n"

    if not isinstance(uid, int):
        raise TypeError(f"create_response_by_id() should be called only with 'int'.\n{values_got}")
    elif not uid:
        raise ValueError(f"create_response_by_id() called with empty ID.\n{values_got}")

    if reasons_per_id is None:
        reasons_per_id = {uid: set()}
    elif isinstance(reasons_per_id, list) or isinstance(reasons_per_id, set):
        if reasons_per_id:
            reasons_per_id = {uid: reasons_per_id}
        else:
            reasons_per_id = {uid: set()}
    ban_reason = ""

    if str(uid).startswith("-100") or uid == ID_channel_bot:
        return texts.id_is_from_channel_no_fban
    if flag_user_got_deleted:
        created_response = texts.parsed_or_mentioned_user_got_deleted
        created_response += f"`{uid}`"
        if uid in already_fbanned_list.PREV_BAN_SET:
            created_response += f"\n`/funban {uid} #deletedPreventiveAccount`"
        elif uid in already_fbanned_list.BAN_SET:
            created_response += f"\n`/funban {uid} #deletedAccount`"
    elif uid in FEDWORKERS:
        created_response = texts.warning_id_of_fedworker
        created_response += f"\n`{uid}`:`{FEDWORKERS[uid]}`"
    elif uid in already_fbanned_list.PREV_BAN_SET:
        try:
            for tag in reasons_per_id[uid]:
                if tag in non_preventive_ban_reasons:
                    ban_reason = f"{ban_reason}#{tag} "
        except KeyError:
            print(f"ID {uid} nicht in Banreasons: {str(reasons_per_id)}")
        created_response = create_already_preventive_fbanned_message(uid, name, username, ban_reason)
    elif uid in already_fbanned_list.BAN_SET:
        created_response = create_already_fbanned_message(uid, name, username)
    else:
        if not default_ban_reason:
            if uid in reasons_per_id:
                for tag in reasons_per_id[uid]:
                    ban_reason = f"{ban_reason}#{tag} "
            created_response = create_fban_message(uid, name, username, ban_reason)
            # Python optimizes this statement by short-circuting, so no manual optimization is needed
            if (5*10**8 <= uid < 8*10**8) or (2*10**8 <= uid < 3*10**8) or (5*10**7 <= uid < 8*10**7) or \
               (5*10**6 <= uid < 8*10**6) or (5*10**6 <= uid < 8*10**6):
                created_response += texts.warning_low_id_identified_by_starting_digit
        else:
            created_response = create_fban_message(uid, name, username, default_ban_reason)
    return created_response


def do_backup_of_banlists_and_create_confirmation_message() -> str:
    """Durchführen des Backups gibt Anzahl der geschriebenen Zeilen zurück"""
    confirmation_message = ""
    number_of_bans = str(already_fbanned_list.write_new_backup_file_from_ban_list())
    if number_of_bans:
        confirmation_message += texts.backup_confirmation
        confirmation_message += number_of_bans
    else:
        confirmation_message += texts.zero_backup_confirmation

    number_of_bans = str(already_fbanned_list.write_new_backup_file_from_prev_ban_list())
    if number_of_bans:
        confirmation_message += texts.backup_of_preventives_confirmation
        confirmation_message += number_of_bans
    else:
        confirmation_message += texts.zero_backup_of_preventives_confirmation
    return confirmation_message


def create_multi_accounts_message(users: Union[dict, list, int], reasons_per_id=None,
                                  responding_to_already_fbanned_id: bool = True,
                                  warn_if_fedworker: bool = True) -> str:
    """Kommt sowohl mit mehreren IDs als auch mit einzelner ID zurecht."""
    if reasons_per_id is None:
        reasons_per_id = {}
    elif isinstance(reasons_per_id, list):
        raise TypeError("create_multi_accounts_message should get reasons_per_id as dict.")

    if isinstance(users, int):
        return create_response_by_id(users, "", False, "", reasons_per_id)
    if isinstance(users, dict):
        return create_multiple_accounts_message_with_dict(users, reasons_per_id,
                                                          responding_to_already_fbanned_id, warn_if_fedworker)
    if isinstance(users, list):
        return create_multiple_accounts_message_with_list(users, reasons_per_id,
                                                          responding_to_already_fbanned_id, warn_if_fedworker)
    raise TypeError("create_multi_accounts_message should get int, list or dict.")


def create_multiple_accounts_message_with_list(list_of_users: list, reasons_per_id=None,
                                               responding_to_already_fbanned_id: bool = True,
                                               warn_if_fedworker: bool = True) -> str:
    if reasons_per_id is None:
        reasons_per_id = {}
    text = ""
    didnt_respond_to_all_ids = False
    counter_for_readability = 0
    list_of_ids_done = []

    for uid in list_of_users:
        if uid in list_of_ids_done:
            # duplicate_ids_found = True      # erstmal nix mit machbar, weil sicherlich häufig vorkommend
            continue
        list_of_ids_done.append(uid)

        if uid in FEDWORKERS:
            if not warn_if_fedworker:
                continue
            text += f"{uid}: {texts.warning_id_of_fedworker}"
            counter_for_readability = 0     # reset
        # If an uid is in PREV_BAN_SET, it is automatically also in BAN_SET
        elif uid not in already_fbanned_list.BAN_SET or uid in already_fbanned_list.PREV_BAN_SET or \
                uid in already_fbanned_list.BAN_SET and responding_to_already_fbanned_id:
            try:
                # die Paramter                     "", False, ""   sind die Default-Werte, müssen aber übergeben werden
                text += create_response_by_id(uid, "", False, "", {uid: reasons_per_id[uid]})
            except KeyError:
                text += create_response_by_id(uid)
            counter_for_readability += 1
            if counter_for_readability == 3:
                text += "\n - - -\n"
                counter_for_readability = 0  # reset
        else:
            didnt_respond_to_all_ids = True
            continue
        text += spacer_for_readability

    if text.endswith(spacer_for_readability):
        text = text[:len(text) - len(spacer_for_readability)]

    if didnt_respond_to_all_ids:
        text = f"{texts.already_fbanned_ids_are_not_commented}\n{text}"
        text = text.rstrip()

    return text


def create_multiple_accounts_message_with_dict(uid_and_name: dict, reasons_per_id=None,
                                               responding_to_already_fbanned_id: bool = True,
                                               warn_if_fedworker: bool = True) -> str:
    if reasons_per_id is None:
        reasons_per_id = {}
    text = ""
    didnt_respond_to_all_ids = False
    counter_for_readability = 0
    list_of_ids_done = []

    for uid in uid_and_name:
        if uid in list_of_ids_done:
            # duplicate_ids_found = True      # erstmal nix mit machbar, weil sicherlich häufig vorkommend
            continue
        list_of_ids_done.append(uid)

        if uid in FEDWORKERS:
            if not warn_if_fedworker:
                continue
            text += texts.warning_id_of_fedworker
            text += f"\n`{uid}`:`{FEDWORKERS[uid]}`"
            counter_for_readability = 0  # reset
        # Entweder nicht gebannt, oder alle Fälle darstellen
        elif uid not in already_fbanned_list.BAN_SET or responding_to_already_fbanned_id:
            first_name_for_current_UID = uid_and_name[uid]
            if not first_name_for_current_UID:
                flag_user_got_deleted = True
                counter_for_readability = 0     # reset
            else:
                flag_user_got_deleted = False
                counter_for_readability += 1
            if counter_for_readability == 3:
                text += "\n - - -\n"
                counter_for_readability = 0     # reset

            try:
                text += create_response_by_id(uid, uid_and_name[uid], flag_user_got_deleted,
                                              "", {uid: reasons_per_id[uid]})
            except KeyError:
                text += create_response_by_id(uid, uid_and_name[uid], flag_user_got_deleted)

        else:
            didnt_respond_to_all_ids = True
            continue

        text += spacer_for_readability

    if text.endswith(spacer_for_readability):
        text = text[:len(text) - len(spacer_for_readability)]

    if didnt_respond_to_all_ids:
        text = f"{texts.already_fbanned_ids_are_not_commented}\n{text}"
        text = text.rstrip()

    return text


# noinspection PyUnusedLocal
def handler_SIGINT_printing_catchtime(signal_received, frame) -> None:
    print("\n", datetime.utcnow(), texts.console__sigint_notification)
    print(texts.confirm_sigint)

    global did_backup_after_sigint__global
    global RUNNING_global
    RUNNING_global = False  # aktueller Durchlauf mit abgerufenen Updates wird noch abgearbeitet, weil sonst verloren
    confirm_sigint = "Backup wurde angestoßen, aber offenbar nicht durchgeführt. " \
                     "(This str will be overwritten by backup process.)"

    bot.send_message(texts.confirm_sigint, bot.MAINCHAT_ID)
    if already_fbanned_list:  # für den Fall das SIGINT direkt nach dem Start kommt
        # noinspection PyBroadException
        try:
            if not did_backup_after_sigint__global:
                confirm_sigint = do_backup_of_banlists_and_create_confirmation_message()
            did_backup_after_sigint__global = True
        except Exception:
            backup_failed_warning = texts.console__backup_failed
            print(backup_failed_warning)
            print(traceback.format_exc())
            log_to_file(backup_failed_warning, bot.MAIN_LOG, verbose=True)
            log_to_file(traceback.format_exc(), bot.MAIN_LOG, verbose=True)
            return
        else:
            bot.send_message_to_owner(confirm_sigint)
    print(f"\n{texts.console__restart_within_24hours}\n")
    print(f"\n{texts.console__please_wait_polling_timeout}")
    return


# noinspection PyUnusedLocal
def handler_slightly_faster_shutdown(signal_received, frame) -> None:
    # Die meiste Zeit nimmt das Warten auf das Polling-Ende in Anspruch, was nicht mittels Signalen beeinflussbar ist
    # noinspection PyBroadException
    try:
        do_backup_of_banlists_and_create_confirmation_message()
    except Exception:
        log_to_file(texts.console__backup_failed, bot.MAIN_LOG, verbose=True)
        log_to_file(traceback.format_exc(), bot.MAIN_LOG, verbose=True)
    finally:
        exit(129)  # 128 + n = Exit wegen Signal mit Wert n


def has_fedworker_been_fbanned(message_text: str) -> str:
    """Kreiere Warnung, falls bekannte Userid in 'New FedBan'-Logchannel-Nachricht"""
    if "New FedBan" in message_text:
        str_starting_with_id = message_text.split("\nUser ID: ", maxsplit=1)  # eigentlich ist nur [1] starting_with_id
        user_id_as_str = str_starting_with_id[1].split("\nReason: ", maxsplit=1)
        user_id_to_test = int(user_id_as_str[0])
        if user_id_to_test in FEDWORKERS:
            fedworker_id = user_id_to_test
            str_warning_fbanned_fedworker = f"`{fedworker_id}`:{FEDWORKERS[fedworker_id]} " + \
                                            texts.warning_fedworker_has_been_banned
            return str_warning_fbanned_fedworker


# Signal handling
RUNNING_global = True
did_backup_after_sigint__global = False
signal(SIGINT, handler_SIGINT_printing_catchtime)
signal(SIGTERM, handler_slightly_faster_shutdown)
signal(SIGHUP, handler_slightly_faster_shutdown)
signal(SIGABRT, handler_slightly_faster_shutdown)

bot = TelegramBot("spam_federation_helper_bot.cfg")
# Primzahlzerlegt
ID_MissRose_bot = 2 * 2 * 11 * 11 * 53 * 23761
ID_rSophieBot = 13 * 2087 * 32647
ID_vhznet = 5 * 31 * 2063 * 5623
ID_unfu_merged = 2 * 2626185257
ID_sangm = 1504304645 * 0.2

bots_which_are_parsed = [ID_MissRose_bot, ID_rSophieBot, ID_vhznet, ID_unfu_merged, ID_sangm]
ID_banof = 2 * 2 * 13 * 6021017  # nicht geparst, stattdessen wird die Message vor Verarbeitung gekürzt
ID_channel_bot = 2**3 * 3 * 7 * 113 * 7207  # steht in from, wenn sender_chat enthalten (also als Kanal gesendet wurde)
ID_GrAnonyBot = 2 * 2 * 2 * 701 * 194003   # bei anon. Adm.
chats_the_bot_sends_to = set()
chats_the_bot_sends_to.add(bot.MAINCHAT_ID)
chats_the_bot_sends_to.add(bot.BOT_OWNER_ID)
chats_the_bot_sends_to.add(bot.DUPLICATES_CHAT_ID)
id_of_fed_logchannel = bot.MAINCHAT_LOG_ID
FEDWORKERS_FILE = "users_working_for_federation.txt"
FEDWORKERS: dict = read_out_dictionary_file(FEDWORKERS_FILE)
FEDWORKERS_NO_HELP: dict = read_out_dictionary_file("users_experienced_no_help.txt")
try:
    already_fbanned_list = BanList()
except KeyboardInterrupt:
    # KeyboardInterrupt sollte nicht die Backup-Dateien kaputt machen
    bot.send_message_to_owner(texts.warning_early_KeyboardInterrupt)
    exit(130)
default_ban_reason = bot.use_this_ban_reason_in_every_create_fban_message  # überschreibt tags_detected in create_response_by_id

log_to_file("Start running bot.", bot.MAIN_LOG, verbose=True)
daily_backup_done = True
date_last_backup = datetime.utcnow().date().__str__()

already_callbacked_msgids = []

spacer_for_readability = "\n-- -- --\n"
bot_responding_to_userinput = ""


def handler_private_message_from_bot_owner(msg_text: str) -> None:
    global FEDWORKERS
    if msg_text.startswith(texts.command__add_someone_to_fedworkerslist):
        user_id___username = get_argument_of_command(msg_text, texts.command__add_someone_to_fedworkerslist)
        if user_id___username:
            key_and_value_for_whitelist = user_id___username.split(":", maxsplit=1)
            try:
                fedworker_userid = int(key_and_value_for_whitelist[0])
                FEDWORKERS.update({fedworker_userid: key_and_value_for_whitelist[1]})
                append_to_dictionary_file(FEDWORKERS_FILE, user_id___username)
            except ValueError:
                bot.send_message_to_owner("Offenbar keine User-ID (vor Doppelpunkt) übergeben.")
            except (IndexError, IOError):
                fail_message = "Konnte Eintrag nicht an Fedworkers-Datei anhängen."
                print(fail_message)
                print(traceback.format_exc())
                bot.send_message_to_owner(fail_message, reply_to_id)
                bot.send_message_to_owner(traceback.format_exc(), reply_to_id)
            else:
                debug_message = f"`{escape_markdown(user_id___username)}` an Fedworkers-Datei angehängt."
                bot.send_message_to_owner(debug_message, reply_to_id, markdown=True)

        else:
            help_for_command_msg = f"Format {texts.command__add_someone_to_fedworkerslist} USER_ID:FIRST_NAME"
            bot.send_message_to_owner(help_for_command_msg)
    elif msg_text.startswith(texts.command__reload_changes_of_file_with_all_fedworkers):
        # für umgekehrte Reihenfolge: erst permanent hinzugefügt in Datei, dann auslesen
        FEDWORKERS = read_out_dictionary_file(FEDWORKERS_FILE)
        confirmation = "Fedworkers-Datei neu eingelesen"
        confirmation += f"\n{len(FEDWORKERS.keys())} Einträge eingelesen"
        bot.send_message_to_owner(confirmation)
    elif msg_text.startswith(texts.command__do_backup_of_banlist):
        confirmation = do_backup_of_banlists_and_create_confirmation_message()
        bot.send_message_to_owner(confirmation)
    elif msg_text.startswith(texts.command__check_if_id_on_banlist):
        try:
            to_check_user_id = int(get_argument_of_command(msg_text, texts.command__check_if_id_on_banlist))
            print(to_check_user_id)
            if to_check_user_id in already_fbanned_list.PREV_BAN_SET:
                bot.send_message_to_owner("user_id ist in präventiver Banliste",
                                          reply_to_id)
            if to_check_user_id in already_fbanned_list.BAN_SET:
                bot.send_message_to_owner("user_id ist in temporärer Banliste",
                                          reply_to_id)
            else:
                bot.send_message_to_owner("user_id ist !nicht! in temp. Banliste",
                                          reply_to_id)
        except (ValueError, TypeError):
            bot.send_message_to_owner(texts.isoncurrentbanlist_help,
                                      reply_to_id, markdown=True)

    # Die beiden folgenden Befehle werden nur benötigt, wenn Rose
    # und dieser Bot out of sync sind, also abweichende Banlisten haben
    elif msg_text.startswith(texts.old_command_add):
        bot.send_message_to_owner(texts.add2banlist_failed, reply_to_id)
    elif msg_text.startswith(texts.old_command_remove):
        bot.send_message_to_owner(texts.remove_from_banlist_help, reply_to_id, markdown=True)
    elif msg_text == texts.command_reload_libs:
        # nötig damit die veränderte Datei neu eingelesen wird
        # Fälle Stand April 22: neue Fed-Admins
        try:
            testnewfeature(message, reply_to_id)
        except Exception as err:
            print("Test failed, exception ausgelöst.")
            print(err)
        importlib.reload(hidden_variables)
        print("\nReloading done.\n")


def handler_service_message_group(service_message: dict) -> None:
    if "migrate_to_chat_id" in message and chat_id == bot.MAINCHAT_ID:
        # Für eine möglicherweise anstehende Änderung, getestet
        new_chat_id_of_main_chat = int(message["migrate_to_chat_id"])
        chats_the_bot_sends_to.add(new_chat_id_of_main_chat)
        warning__main_chat_id_changed = from__to_readable_str(from_)
        warning__main_chat_id_changed += texts.warning_mainchat_id_has_been_changed
        log_to_file(warning__main_chat_id_changed, bot.MAIN_LOG, verbose=True)
        log_to_file(str(item), bot.MAIN_LOG, verbose=True)
        bot.send_message_to_owner(warning__main_chat_id_changed)
        sleep(0.5)
        bot.change_mainchat_id(new_chat_id_of_main_chat)
        bot.send_message(warning__main_chat_id_changed, message["migrate_to_chat_id"])
    elif "group_chat_created" in message:
        # aber! supergroup_chat_created - "This field can't be received in a message coming through updates,
        # because bot can't be a member of a supergroup when it is created."
        debug_message = f"{texts.bot_was_added_to_new_group}{from__to_readable_str(from_)}{chat__to_readable_str(chat)}"
        bot.send_message_to_owner(debug_message)
        # no bot.leave_chat at this point, might be migrated anyway
    elif "new_chat_members" in service_message and service_message["chat"]["id"] != bot.MAINCHAT_ID:
        # new_chat_participant / new_chat_member ist veraltet
        # zweite Bedingung: falls in Chat hinzugefügt mit vielen anderen -> ignorieren

        for new_user in service_message["new_chat_members"]:
            userid_joined = new_user["id"]
            username_joined = escape_markdown(new_user["first_name"])
            if userid_joined == bot.BOT_ID:
                if chat["type"] == "private":
                    debug_message = f"{texts.private_chat_started}{from__to_readable_str(from_)}\n" \
                                    f"{link_to_TG_profil(chat_id)}"
                else:
                    debug_message = f"{texts.bot_was_added_to_group}{from__to_readable_str(from_)}Chat:\n" \
                                    f"{chat__to_readable_str(chat)}"
                    title_of_chat = chat["title"]  # Immer vorhanden; private Chats haben first_name stattdessen
                    if not (chat_id == bot.DUPLICATES_CHAT_ID):
                        try:
                            if chat_id != bot.MAINCHAT_ID:  # Nur für Start einer Instanz nötig
                                bot.leave_chat(chat_id, chat["username"], title_of_chat)
                        except KeyError:
                            bot.leave_chat(chat_id, title=title_of_chat)
                reply_new_chat_member = debug_message
            elif userid_joined not in FEDWORKERS:
                reply_new_chat_member = create_user_joined_message(userid_joined, username_joined,
                                                                   bot.AUTO_WHITELISTING_NEW_USERS)
                # Automatisches Ignorieren neuer Nutzer,
                # nur temporär, Kommando nötig zum Schreiben in Datei
                if bot.AUTO_WHITELISTING_NEW_USERS:
                    FEDWORKERS.update({userid_joined: username_joined})
            else:
                reply_new_chat_member = create_user_joined_message(userid_joined, username_joined,
                                                                   already_allowed=True)
            bot.send_message_to_owner(reply_new_chat_member, markdown=False)
    elif "left_chat_member" in message:
        id_of_leaving_user = message["left_chat_member"]["id"]
        if not id_of_leaving_user == bot.BOT_ID:
            debug_message = escape_markdown(message["left_chat_member"]["first_name"]) + " "
            if message["from"]["id"] == id_of_leaving_user:
                debug_message += f"`{id_of_leaving_user}` hat Chat {chat_id} verlassen."
            else:
                debug_message += f"`{id_of_leaving_user}` wurde aus Chat {chat_id} entfernt."
            debug_message += "\n" + link_to_TG_profil(id_of_leaving_user)
            bot.send_message_to_owner(debug_message, markdown=True)


def handler_new_entry_in_fed_logchannel():
    message_read_in_fed_channel: dict = item["channel_post"]
    try:
        id_of_channel = message_read_in_fed_channel["sender_chat"]["id"]
        if id_of_channel == id_of_fed_logchannel:
            ban_message = message_read_in_fed_channel["text"]
            warning_to_main_chat = has_fedworker_been_fbanned(ban_message)
            if warning_to_main_chat:
                bot.send_message(warning_to_main_chat, bot.MAINCHAT_ID, markdown=True)
            # noinspection PyUnboundLocalVariable
            warning_msg_from_fed_log_entry = already_fbanned_list.read_new_fed_log_entry(ban_message)
            if warning_msg_from_fed_log_entry:
                log_to_file(warning_msg_from_fed_log_entry, bot.MAIN_LOG, verbose=True)
        else:
            ignore_message(bot.LOG_IGNR, message_read_in_fed_channel, "wrongchannel")
            try:
                bot.leave_chat(id_of_channel, message_read_in_fed_channel["sender_chat"]["username"])
            except KeyError:
                bot.leave_chat(id_of_channel)
    except KeyError:
        if "new_chat_title" not in message_read_in_fed_channel:
            # dann ist es kein regulärer channel_post -> loggen
            ignore_message(bot.LOG_IGNR, message_read_in_fed_channel, "chanpost_exc")
            log_to_file(texts.log__logchannel_unknown_msg, bot.MAIN_LOG, verbose=True)
        else:
            log_to_file(texts.log__logchannel_renamed, bot.MAIN_LOG, verbose=True)


def handler_callback_query(callback):
    if callback["callback_query"]["data"] == "Delete":
        try:
            if callback["callback_query"]["message"]["chat"]["id"] not in [bot.MAINCHAT_ID, bot.BOT_OWNER_ID] or \
                    callback["callback_query"]["from"]["id"] not in FEDWORKERS:
                weird_things_happened = f'>Delete< callback empfangen, aber aus falschem Chat' \
                                        f' {callback["callback_query"]["message"]["chat"]["id"]} oder ' \
                                        f'von unbekanntem Nutzer {callback["callback_query"]["from"]["id"]}.'
                log_to_file(weird_things_happened, bot.MAIN_LOG, verbose=True)
                print(callback)
                log_to_file(str(callback), bot.MAIN_LOG, verbose=True)
            elif callback["callback_query"]["message"]["message_id"] not in already_callbacked_msgids:
                already_callbacked_msgids.append(callback["callback_query"]["message"]["message_id"])
                bot.delete_message(callback["callback_query"]["message"]["message_id"],
                                   callback["callback_query"]["message"]["chat"]["id"])
                bot.answerCallbackQuery(callback["callback_query"]["id"])
        except KeyError:
            weird_things_happened2 = ">Delete< callback empfangen, allerdings KeyError."
            log_to_file(weird_things_happened2, bot.MAIN_LOG, verbose=True)
            print(callback)
            log_to_file(str(callback), bot.MAIN_LOG, verbose=True)
    else:
        weird_things_happened3 = f'Unbekannter callback value empfangen:\n{callback["callback_query"]}'
        log_to_file(weird_things_happened3, bot.MAIN_LOG, verbose=True)
        print(callback)


while RUNNING_global:
    sekunden_UTCnow = int(datetime.utcnow().__str__()[17:19])
    if sekunden_UTCnow > 55:
        # willkürliche Reduktion von print; geprüft werden die Sekunden; .strftime() funzt iwie nicht zuverlässig
        print(datetime.utcnow().__str__()[:16])
        if not daily_backup_done:
            confirmation_of_backup = do_backup_of_banlists_and_create_confirmation_message()
            bot.send_message_to_owner(confirmation_of_backup)

            daily_backup_done = True
            date_last_backup = datetime.utcnow().date().__str__()

        elif date_last_backup != datetime.utcnow().date().__str__():
            daily_backup_done = False
    time_to_wait_in_seconds_before_getting_update = 0.25
    sleep(time_to_wait_in_seconds_before_getting_update)
    bot.get_updates(timeout=60)

    get_updates_failed, wait_time = bot.get_updates_failed()

    if get_updates_failed:
        # Log wird in der Klasse geschrieben
        wait_in_seconds_before_retry__get_update = wait_time
        sleep(wait_in_seconds_before_retry__get_update)
        if bot.debug:
            print(f"{texts.debug__prefix}{texts.debug__get_updates_failed}")
            print(bot.updates)
        continue
    updates = bot.updates["result"]  # der JSON-Anteil, der relevant ist

    # noinspection PyBroadException
    try:
        for item in updates:
            bot.update_id = item["update_id"]
            # Channel sind anders kodiert, daher wird der Code dafür vorher abgearbeitet
            if "channel_post" in item:
                # nur wenn Bot in Channel mitliest; nicht bei forward eines Channel-Posts
                handler_new_entry_in_fed_logchannel()
                continue

            if "callback_query" in item:
                handler_callback_query(item)
                continue

            if "my_chat_member" in item:
                # durch allowed_updates=...my_chat_member möglich
                # private Chat: Block oder Unblock durch den Nutzer
                # Gruppe: Kick aus Gruppe -> enthält kein item["message"] -> muss vorher abgearbeitet werden
                log_message(bot.LOG_MSGS, item)
                continue

            message: dict = item["message"]
            chat: dict = message["chat"]
            chat_id: int = chat["id"]           # ! should not be overwritten, because used for delete_message !
            from_: dict = message["from"]
            from_userid: int = from_["id"]        # in der Regel Fedworker
            reply = ""
            reason_tags_by_uid = dict()
            reason_tags_if_single_uid = dict()

            if stop_iteration(message):
                continue
            if bot.debug:
                print(item)
            log_message(bot.LOG_MSGS, message)

            # ...update - Um KeyError zu vermeiden bzw. Code zu vereinfachen
            if "text" not in message:
                message.update({"text": ""})
            if "message_id" not in message:
                message.update({"message_id": 0})
                if bot.debug:
                    bot.send_message_to_owner(texts.warning_broken_message__no_message_id)
            reply_to_id: int = message["message_id"]

            delay_until_update_received__in_seconds = int(time() - message["date"])
            update_considered_outdated__in_seconds = 15 * 60
            update_ignored_after__in_seconds = 60 * 60

            if delay_until_update_received__in_seconds > update_ignored_after__in_seconds:
                warning_msg_updates_delayed = f"{texts.warning_delay_detected}{delay_until_update_received__in_seconds}" \
                                              f"{texts.warning_delay_detected2}"
                if not bot.delay_detected_and_reported:
                    # ungewöhnliches Verhalten (hier: ausgebliebene/verzögerte Reaktion) soll sichtbar sein
                    bot.send_message_to_owner(warning_msg_updates_delayed)
                print(warning_msg_updates_delayed)
                bot.delay_detected_and_reported = True
                continue
            if delay_until_update_received__in_seconds > update_considered_outdated__in_seconds:
                bot.SEND_MESSAGES_TO_DUPLICATES_CHAT = True
            else:
                bot.SEND_MESSAGES_TO_DUPLICATES_CHAT = False
                bot.delay_detected_and_reported = False

            if "new_chat_members" in message or ("migrate_to_chat_id" in message and chat_id == bot.MAINCHAT_ID)\
                    or "group_chat_created" in message or "left_chat_member" in message:
                handler_service_message_group(message)
                continue

            if "forward_from_chat" in message:
                reply_forward_from_chat = ""
                user_got_deleted = False
                parsed_users = {}
                reason_tags_by_uid = {}
                # Weiterleitung aus Kanal
                # Ist aus dem Logchannel der Fed?       -> Nachreichung von IDs, z.B. nach Crash
                # Ist aus einem speziellen Newschannel? -> Ignorieren
                # Ist aus einem Roselog-Channel?        -> ID und Name parsen
                # Ist aus einem zu parsenden Channel?   -> Mentions checken
                # sonst Standard-Beantwortung
                if message["forward_from_chat"]["id"] == id_of_fed_logchannel:
                    # nach Crash verpasste Channel-Logeinträge nachträglich einlesen
                    if chat["type"] == "private" and from_userid == bot.BOT_OWNER_ID:
                        print(texts.console__read_again_log_entry)
                        # noinspection PyUnboundLocalVariable
                        warning_msg = already_fbanned_list.read_new_fed_log_entry(message["text"])
                        if warning_msg:
                            print(warning_msg)
                            log_to_file(warning_msg, bot.MAIN_LOG, verbose=True)
                    # Sonst: ignorieren von Anti Spam Log Channel
                    continue
                elif message["forward_from_chat"]["id"] in hidden_variables.set_of_ignored_channels:
                    # Informative Weiterleitungen, z.B. aus dem News-Channel von Rose, werden nicht kommentiert
                    continue

                is_some_fed_log, parsed_uid_from_fedlog, parsed_name_from_fedlog =\
                    parsing.rose_fedlog_check_and_get_id(message)
                if is_some_fed_log:
                    reply_forward_from_chat = create_fban_message(parsed_uid_from_fedlog, parsed_name_from_fedlog)
                elif message["text"].startswith("#"):
                    parsed_users, reason_tags_by_uid = parsing.rose_log_check_and_get_id(message)
                    if parsed_users:
                        reply_forward_from_chat = create_multiple_accounts_message_with_dict(parsed_users, reason_tags_by_uid)

                if not reply_forward_from_chat:
                    if message["forward_from_chat"]["id"] in hidden_variables.set_of_channels_to_parse:
                        mentioned_users_in_channel_post, reason_tags_by_uid = dict_of_users_in_msg_entities(message)
                        if mentioned_users_in_channel_post:
                            reply_forward_from_chat += create_multi_accounts_message(mentioned_users_in_channel_post,
                                                                                     reason_tags_by_uid)
                    elif not (from_userid in FEDWORKERS_NO_HELP):
                        # sonst Standard-Antwort
                        reply_forward_from_chat = texts.forward_from_chat
                        bot.send_deletable_message(reply_forward_from_chat, "Ok, Hinweis löschen.",
                                                   chat_id, reply_to_id, markdown=1)
                        continue

                else:
                    bot.send_message(reply_forward_from_chat, chat_id, reply_to_id, markdown=True)
                continue

            if "forward_sender_name" in message:
                # "Das Konto wurde vom Nutzer ausgeblendet."
                username_not_linkable_by_id = escape_markdown(message["forward_sender_name"])
                if username_not_linkable_by_id == "Deleted Account":
                    reply_forward_sender_name = texts.forwarded_user_got_deleted
                    bot.send_deletable_message(reply_forward_sender_name, "Hinweis löschen",
                                               chat_id, reply_to_id, markdown=True)
                    continue

                reply_forward_sender_name = f"Nutzername: `{username_not_linkable_by_id}`"
                minimum_length_for_team_member_check = 2
                if len(username_not_linkable_by_id) > minimum_length_for_team_member_check:
                    if username_not_linkable_by_id in FEDWORKERS.values():
                        reply_forward_sender_name += texts.user_disabled_forward_ID_username_in_this_group
                reply_forward_sender_name += texts.user_disabled_forward_ID

                mentioned_users_in_uidless_message, reason_tags_by_uid = dict_of_users_in_msg_entities(message)
                if mentioned_users_in_uidless_message:
                    reply_forward_sender_name += texts.user_disabled_forward_ID_but_mentioned_user
                    reply_forward_sender_name += create_multi_accounts_message(mentioned_users_in_uidless_message,
                                                                               reason_tags_by_uid)
                bot.send_message(reply_forward_sender_name, chat_id, reply_to_id, markdown=True)
                continue

            # Abschnitt zu Kommandos von Nutzern (privat und in Main)
            if message["text"].startswith(texts.command__all_users__ping):
                # Die Zeitdifferenz und Abfragezeit anzuzeigen hat auch den Vorteil,
                # dass die Antwort nicht als Duplicate unterdrückt wird
                time_passed_since_request_in_sec = round(time() - message['date'], kommastellen := 0)
                bot_responding_to_userinput = f"{texts.response_bistduonline}" \
                                              f"Pingtime >= {time_passed_since_request_in_sec} Sek.\n" \
                                              f"Abfrage gesendet: {datetime.utcfromtimestamp(message['date'])}"

            elif message["text"].startswith(texts.command__all_users__help_forwarded_message):
                bot_responding_to_userinput = texts.forward_from_chat_details

            if bot_responding_to_userinput:
                if chat["type"] == "private" and from_userid in FEDWORKERS:
                    target_chat_id = from_userid
                else:
                    target_chat_id = chat_id
                bot.send_message(bot_responding_to_userinput, target_chat_id, reply_to_id, markdown=True)
                bot_responding_to_userinput = ""
                continue

            if (from_userid not in chats_the_bot_sends_to) and \
               (chat_id not in chats_the_bot_sends_to):
                # Bot nur in Mainchat nutzbar
                if chat["type"] == "private":
                    ignore_message(bot.LOG_IGNR, message, "private_Chat")
                else:
                    # Dieser Code wird nur wirksam in spezieller Situation: 1. Bot offline
                    # 2. wird währenddessen in Chat hinzugefügt 3. empfängt Nachricht
                    # 4. Bot wird gestartet und arbeitet diese Nachrichten ab
                    ignore_message(bot.LOG_IGNR, message, "wrong__Group")
                    try:
                        bot.leave_chat(chat_id, chat["username"])
                    except KeyError:
                        bot.leave_chat(chat_id)
                continue

            if chat["type"] == "private" and from_userid == bot.BOT_OWNER_ID:
                handler_private_message_from_bot_owner(message["text"])
                continue

            if chat["type"] == "private":
                # Nutzung des Bots per PN nicht möglich
                if chat_id != bot.BOT_OWNER_ID:
                    continue

            if message["text"].startswith(texts.command__check_msg_for_id):
                if from_userid not in FEDWORKERS:
                    # unwahrscheinlicher Fall, da PN-Chats vorher abgefangen wurden
                    raise ValueError(f"{texts.command__check_msg_for_id} should not be able to use in private chat.")
                if "reply_to_message" in message:
                    user_id_from_check_up, reason_tags_by_uid = parsing.check_id_in_text(message["reply_to_message"])
                    if user_id_from_check_up:
                        reply_to_checkup = create_multi_accounts_message(user_id_from_check_up, reason_tags_by_uid)

                        # Nachrichten, die nicht/präventiv-gebannt-Meldung enthalten -> sollen nicht löschbar sein
                        if (texts.user_id_is_banned_preventive in reply_to_checkup) or \
                           (texts.user_id_not_banned in reply_to_checkup):
                            bot.send_message(reply_to_checkup, chat_id, reply_to_id, markdown=True)
                    else:
                        reply_to_checkup = texts.check_id_in_text__no_id_found
                else:
                    reply_to_checkup = texts.check_id_in_text__help

                bot.send_deletable_message(reply_to_checkup, "Ok, Hinweis löschen.", chat_id, reply_to_id)
                continue

            # 2. Teil "and not "forward_from" in message" bezieht sich auf ID_unfu_merged und mglw andere Bots
            if message["text"].startswith("/info ") and "forward_from" not in message:
                if message["text"].startswith("/info @"):
                    # Falls erst @username und dann die ID gepostet,
                    # sollte das durch ein weiteres Leerzeichen erkennbar sein
                    if message["text"].find(" ", len("/info @")) == -1:
                        continue
                if from_userid not in FEDWORKERS:
                    # unwahrscheinlicher Fall, da PN-Chats vorher abgefangen wurden
                    raise ValueError(" should not be able to use in private chat.")
                list_of_ids_of_info_cmd, reason_tags_if_single_uid = parsing.extract_ids_or_id_and_reasons(message["text"][6:])
                reply_to_info_cmd = create_multi_accounts_message(list_of_ids_of_info_cmd, reason_tags_if_single_uid)
                if reply_to_info_cmd:
                    if len(list_of_ids_of_info_cmd) == 1:
                        if texts.user_id_is_banned not in reply_to_info_cmd:
                            # Spezialfall "Schon gebannt" wird anders kommentiert
                            reply_to_info_cmd += " (Bitte auf Rose antworten und 👍.)"
                    bot.send_deletable_message(reply_to_info_cmd, "Ok, Hinweis löschen.",
                                               chat_id, reply_to_id, markdown=True)
                    continue

            if message["text"] == f"/fban@{bot.BOT_USERNAME}":
                if bot.BOT_USERNAME:
                    # Löscht nur, wenn der Parameter wirklich gesetzt ist und nicht ein leerer String
                    bot.delete_message(message["message_id"], chat_id)

            # Weitergeleitete Nachricht? Vereinfacht den nachfolgenden Code über die Variable "message_header"
            # Sobald forward_from in der Nachricht existiert, wird nur dieser Teil untersucht.
            if "forward_from" in message:
                message_header: dict = message["forward_from"]
                if message_header["is_bot"]:
                    id_str_from_bot_msg = ""
                    name_from_bot_msg = ""
                    username_from_bot_msg = ""
                    user_got_deleted = False

                    bot_id = message_header["id"]

                    if bot_id == ID_MissRose_bot:
                        # id_str_from_bot_msg in diesem Fall schon korrekt int, also gleich id_from_bot_message
                        is_some_fed_log, id_str_from_bot_msg, name_from_bot_msg = \
                            parsing.rose_fedlog_check_and_get_id(message)
                        if not is_some_fed_log:
                            id_str_from_bot_msg, name_from_bot_msg = parsing.rose_response_to_command(message["text"])
                    elif bot_id == ID_rSophieBot:
                        id_str_from_bot_msg, name_from_bot_msg = parsing.sophi_response_to_command(message["text"])
                    elif bot_id == ID_vhznet:
                        id_str_from_bot_msg, name_from_bot_msg, username_from_bot_msg = \
                            parsing.vhznet_rename_or_voteban_or_deleted_account(message["text"])
                        # wenn der Account verlinkt ist, sollte das Parsen das Auslesen nicht verhindern
                        mentioned_users_in_vhznet_msg, _ = dict_of_users_in_msg_entities(message)
                        for uid_vhznet in mentioned_users_in_vhznet_msg:
                            if str(uid_vhznet) == id_str_from_bot_msg:
                                name_from_bot_msg = mentioned_users_in_vhznet_msg[uid_vhznet]   # falls Account gelöscht
                                break
                    elif bot_id == ID_banof:
                        message = parsing.banof__remove_entities_of_non_spammers(message)
                    elif bot_id == ID_unfu_merged:
                        relevant_txt = ""
                        if "caption" in message:
                            relevant_txt = message["caption"]
                        else:
                            relevant_txt = message["text"]
                        id_str_from_bot_msg, name_from_bot_msg, username_from_bot_msg, reason_tags_by_uid = \
                            parsing.unfu_merged(relevant_txt)
                    elif bot_id == ID_sangm:
                        id_str_from_bot_msg, name_from_bot_msg, username_from_bot_msg, reason_tags_by_uid = \
                            parsing.sangm_rename(message["text"])

                    else:
                        _, reason_tags_by_uid = dict_of_users_in_msg_entities(message)

                    if id_str_from_bot_msg:
                        try:
                            id_from_bot_message = int(id_str_from_bot_msg)
                        except ValueError as e:
                            print(traceback.format_exc())
                            bot.send_message_to_owner(texts.no_user_found)
                            bot.send_message_to_owner(message["text"])
                            bot.send_message_to_owner(traceback.format_exc())
                            # Dieser Zweig läuft weiter.
                            # (z.B. nicht parse-bare Log-Channel-Nachrichten)
                        else:
                            if not name_from_bot_msg:
                                user_got_deleted = True

                            reply_bot_parsed = create_response_by_id(id_from_bot_message, name_from_bot_msg,
                                                                     user_got_deleted, username_from_bot_msg,
                                                                     reason_tags_by_uid)
                            bot.send_message(reply_bot_parsed, chat_id, reply_to_id, markdown=True)
                            # Dieser Zweig endet, aber der except-Zweig nicht -> Continue korrekt eingerückt
                            continue
                    elif message_header["id"] not in bots_which_are_parsed:
                        # Weiterleitung von unbekannten Bot: wird noch nicht geparst oder zum Bann vorgeschlagen
                        # ! Die einzige Reply-Ergänzung, die zu späteren Abschnitten beiträgt!
                        # Muss evtl. später abgefangen werden -> message_to_send_is_effectively_empty()
                        reply = texts.message_from_bot__title

            elif "from" in message:
                message_header: dict = message["from"]
                if message_header["is_bot"] and message_header["id"] != ID_GrAnonyBot:
                    # Normalerweise unmöglich (Bots lesen keine Bot-Nachrichten)
                    # Messages von Rose/Bots werden nicht geloggt
                    raise ValueError("""Bots shouldn't see message["from"]["is_bot"]=True""")

            else:
                ignore_message(bot.LOG_IGNR, message, "wrong__type")
                raise KeyError("""Message doesn't have forward_from nor from.""")

            user_id: int = message_header["id"]
            user_first_name = escape_markdown(message_header["first_name"])
            if user_id in FEDWORKERS:
                # 1. Hat caption?
                # 2. Hat Mentions im Fließtext?
                reply_to_msg_from_fedworker = ""
                comment_on_already_fbanned_users = True

                if "caption" in message:
                    list_of_ids_in_caption, reason_tags_if_single_uid = parsing.extract_ids_or_id_and_reasons(message["caption"])
                    if not list_of_ids_in_caption:
                        if not (message["caption"].startswith("/fban")):
                            ignore_message(bot.LOG_IGNR, message["caption"], "unknown_cap")
                        continue

                    if user_id in hidden_variables.set_of_fedadmin:
                        # kein unnötiges Kommentieren bei Nachreichung durch Fedadmins
                        comment_on_already_fbanned_users = False
                    reply_to_msg_from_fedworker = create_multi_accounts_message(list_of_ids_in_caption,
                                                                                reason_tags_if_single_uid,
                                                                                comment_on_already_fbanned_users)

                    if not reply_to_msg_from_fedworker or \
                       reply_to_msg_from_fedworker == texts.already_fbanned_ids_are_not_commented:
                        bot.send_deletable_message(texts.all_IDs_in_caption_have_been_banned, "Ok, Hinweis löschen.",
                                                   bot.MAINCHAT_ID, reply_to_id)
                        continue
                else:
                    mentioned_users_in_report, reason_tags_by_uid = dict_of_users_in_msg_entities(message)
                    if mentioned_users_in_report:
                        reply_to_msg_from_fedworker += create_multi_accounts_message(mentioned_users_in_report,
                                                                                     reason_tags_by_uid)
                if reply_to_msg_from_fedworker:
                    bot.send_message(reply_to_msg_from_fedworker, chat_id, reply_to_id, markdown=True)
                continue

            # allen übrigen Nachrichten:
            # Weitergeleitete Nachrichten von Nutzern mit unbekannter ID
            #
            # _Heuristik_ um Spamnachrichten mit Mention von Nutzer-Meldungen zu unterscheiden:
            # Nutzer-Meldungen sind kurz, Spamnachrichten in diesem Fall lang -> Warnung nur bei kurzen Nachrichten
            minimum_length_of_spam_message_with_user_mention = 100
            list_of_words_marking_non_spammers = ["@admin", "hinzugefügt", "ungefragt", "irgendein", "bitte bannen",
                                                  "bitte kicken", "bitte rausschmeißen", "danke admins", "achtung"]
            could_be_message_from_non_spammer = False

            if any(word in message["text"].lower() for word in list_of_words_marking_non_spammers):
                could_be_message_from_non_spammer = True
            elif "caption" in message:
                if len(message["caption"]) < 25:
                    could_be_message_from_non_spammer = True

            if len(message["text"]) < minimum_length_of_spam_message_with_user_mention or \
                    could_be_message_from_non_spammer:
                mentioned_users_in_non_fedworker_msg, reason_tags_by_uid = dict_of_users_in_msg_entities(message)
                only_usernames_mentioned = only_usernames_mentioned__no_user_linked(message)
                if mentioned_users_in_non_fedworker_msg or only_usernames_mentioned:
                    reply += texts.warning_reporting_user_and_their_mention
                    if not only_usernames_mentioned:
                        reply += create_multi_accounts_message(mentioned_users_in_non_fedworker_msg,
                                                               reason_tags_by_uid) + "\n"
                elif could_be_message_from_non_spammer:
                    reply = f"{texts.warning_could_be_non_spammer}\n{reply}"
# reason_tags: Dict[int, Set] = check_user_object_for_ban_reasons(message_header)
            reason_tags: Dict[int, Set] = check_user_object_for_ban_reasons(message_header)
            # Gelöschte Konten werden über "forward_sender_name" abgearbeitet, daher hier immer False
            reply += create_response_by_id(user_id, user_first_name, False, "", reason_tags)
            if reply:
                bot.send_message(reply, chat_id, reply_to_id, markdown=True)
    except KeyboardInterrupt:
        handler_SIGINT_printing_catchtime(SIGINT, None)
    except SystemExit:
        handler_slightly_faster_shutdown(SIGKILL, None)
    except Exception:
        print(traceback.format_exc())
        log_to_file(texts.log__unknown_expection, bot.MAIN_LOG, verbose=True)
        # noinspection PyBroadException
        # @noinspection - das ist der Hauptzweig und so -> alles abfangen
        try:
            if "text" in item["message"]:
                # noinspection PyUnboundLocalVariable
                # dafür ist ja der Try-Catch-Block da...
                log_to_file(item["message"]["text"], bot.MAIN_LOG, verbose=True)
            else:
                log_to_file(item["message"], bot.MAIN_LOG, verbose=True)
        except NameError or KeyError:
            pass
        log_to_file(traceback.format_exc(), bot.MAIN_LOG, verbose=True)
        bot.send_message_to_owner(texts.warning_unknown_exception)
        bot.send_message_to_owner(traceback.format_exc())

print(texts.console__sigint_finished, datetime.utcnow().__str__()[:19])
sys_exit(0)
