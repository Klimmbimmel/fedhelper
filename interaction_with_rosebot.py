# -*- coding: utf-8 -*-

from datetime import datetime
from json import loads
import os
import lib_spam_federation_helper_bot_textstrings as texts


class BanList:
    """Creating a set of banned users from Rosebot exports or log msg"""

    def __init__(self):
        self.debug = False
        self.NUMBER_OF_BANS = 0
        self.NUMBER_OF_PREV_BANS = 0
        self.FILENAME_EXPORTED_BAN_LIST_JSON = "fbanned_users.json"
        self.FILENAME_BACKUP = "current_fed_banned_users_one_id_per_line.txt"
        self.FILENAME_BACKUP_PREVENTIVE_BANS = "current_preventive_bans.txt"
        self.BAN_SET = set()  # sets are faster when checking membership: x in set O(1), x in list O(n)
        self.PREV_BAN_SET = set()
        if not os.path.exists(self.FILENAME_BACKUP):
            print(texts.console__no_backup_file_of_bans_reading_export)
            self.no_backup_so_readout_ban_list__json()
        else:
            print(texts.console__backup_found)
            self.readout_backup(readout_preventives=False)
        if not os.path.exists(self.FILENAME_BACKUP_PREVENTIVE_BANS):
            print(texts.console__no_preventives_backup_file_of_bans)
        else:
            print(texts.console__preventives_backup_found)
            self.readout_backup(readout_preventives=True)
        if self.debug:
            print(texts.console__verbose_init_finished)

    def readout_backup(self, readout_preventives: bool) -> None:
        if readout_preventives:
            file = self.FILENAME_BACKUP_PREVENTIVE_BANS
            set_of_ids = self.PREV_BAN_SET
        else:
            file = self.FILENAME_BACKUP
            set_of_ids = self.BAN_SET
        number_of_bans = 0
        with open(file, "r") as backup:
            lines_in_file = backup.readlines()
            for line in lines_in_file:
                line = line.rstrip()    # nötig um den Zeilenumbruch zu löschen
                try:
                    set_of_ids.add(int(line))
                except ValueError:
                    print(texts.console__broken_line_in_backup_file)
                else:
                    number_of_bans += 1
        print(f"{number_of_bans} Datensätze ausgelesen. (Backup-Datei)")
        if readout_preventives:
            self.NUMBER_OF_PREV_BANS = number_of_bans
        else:
            self.NUMBER_OF_BANS = number_of_bans

    def no_backup_so_readout_ban_list__json(self) -> None:
        if not os.path.exists(self.FILENAME_EXPORTED_BAN_LIST_JSON):
            print(texts.console__no_export_file_found)
            return
        # errors="ignore" verhindert Exception UnicodeDecodeError
        with open(self.FILENAME_EXPORTED_BAN_LIST_JSON, "r",
                  encoding="utf-8", errors="ignore") as json_file:
            lines_in_file = json_file.readlines()
            for line in lines_in_file:
                line = line.rstrip()    # nötig um den Zeilenumbruch zu löschen
                json = loads(line)
                try:
                    if "preventiv" in json["reason"] or "Preventiv" in json["reason"] or "präventiv" in json["reason"]:
                        self.PREV_BAN_SET.add(int(json["user_id"]))
                    self.BAN_SET.add(int(json["user_id"]))
                except (ValueError, KeyError) as e:
                    print(texts.console__broken_line_in_export_file)
                    print(e)
                    continue
                else:
                    self.NUMBER_OF_BANS += 1
        self.write_new_backup_file_from_ban_list()
        print(f"{self.NUMBER_OF_BANS} Datensätze ausgelesen. (JSON-Export)")

    def read_new_fed_log_entry(self, message_text: str) -> str:
        message_text = message_text.replace("Preventiv", "preventiv")
        message_text = message_text.replace("präventiv", "preventiv")
        if "New FedBan" in message_text:
            tmp = message_text.split("\nUser ID: ", maxsplit=1)
            tmp = tmp[1].split("\nReason: ", maxsplit=1)
            user_id = int(tmp[0])
            if "preventiv" in message_text:
                if user_id not in self.PREV_BAN_SET:
                    self.PREV_BAN_SET.add(user_id)
            if user_id not in self.BAN_SET:
                self.append_user_id(user_id)
            else:
                warning_id_known = f"Achtung: Obwohl >New Fedban< war die ID schon bekannt\nID: {user_id}"
                print(warning_id_known)
                return warning_id_known
        elif "New un-FedBan" in message_text:
            tmp = message_text.split("\nUser ID: ", maxsplit=1)
            tmp = tmp[1].split("\nReason: ", maxsplit=1)
            user_id = int(tmp[0])
            if user_id in self.PREV_BAN_SET:
                self.PREV_BAN_SET.remove(user_id)
            if user_id in self.BAN_SET:
                self.remove_user_id(user_id)
            elif self.debug:
                warning_id_unknown = f"ID {user_id} aus un-FedBan nicht in Banlist \n"
                print(warning_id_unknown)
                return warning_id_unknown
        elif "FedBan Reason update" in message_text:
            warnings_to_return = ""
            tmp = message_text.split("\nUser ID: ", maxsplit=1)
            tmp = tmp[1].split("\nPrevious Reason: ", maxsplit=1)
            user_id = int(tmp[0])
            if user_id not in self.BAN_SET:
                # Integritätsprüfung
                self.append_user_id(user_id)
                if self.debug:
                    warning_id_unknown = f"ID {user_id} aus FedBan Reason update nicht in Banlist \n"
                    print(warning_id_unknown)
                    warnings_to_return = warning_id_unknown

            if not ("preventiv" in tmp[1]):
                return warnings_to_return
            tmp = tmp[1].split("\nNew Reason: ", maxsplit=1)
            if "preventiv" in tmp[0] and not ("preventiv" in tmp[1]):
                # Umwandlung von präventiven in regulären Ban
                try:
                    self.PREV_BAN_SET.remove(user_id)
                except (ValueError, KeyError) as e:
                    warning_id_not_removable = f"ID {user_id} wurde nicht auf PREV_BAN_SET zum Entfernen gefunden."
                    print(warning_id_not_removable)
                    print(e)
                    warnings_to_return += warning_id_not_removable
                    return warnings_to_return
            elif "preventiv" in tmp[1] and not ("preventiv" in tmp[0]):
                # Nachtrag, dass es sich um einen präventiven Ban handelt
                self.PREV_BAN_SET.add(user_id)
            return warnings_to_return

    def append_user_id(self, user_id: int) -> None:
        if user_id not in self.BAN_SET:
            self.BAN_SET.add(user_id)

    def remove_user_id(self, user_id: int) -> None:
        try:
            self.BAN_SET.remove(user_id)
        except (KeyError, ValueError):
            if self.debug:
                print(texts.console__verbose_not_able_to_remove_user_id_from_BAN_SET)

    def write_new_backup_file_from_ban_list(self) -> int:
        if self.BAN_SET:
            if os.path.exists(self.FILENAME_BACKUP):
                self.rename_backup()
            with open(self.FILENAME_BACKUP, "w") as new_backup:
                zeilen_geschrieben = 0
                for element in self.BAN_SET:
                    new_backup.writelines(str(element) + "\n")
                    zeilen_geschrieben += 1
            print("Neues Backup geschrieben")
            return zeilen_geschrieben
        print("Kein Backup geschrieben")
        return 0

    def write_new_backup_file_from_prev_ban_list(self) -> int:
        if self.PREV_BAN_SET:
            with open(self.FILENAME_BACKUP_PREVENTIVE_BANS, "w") as new_backup:
                zeilen_geschrieben = 0
                for element in self.PREV_BAN_SET:
                    new_backup.writelines(str(element) + "\n")
                    zeilen_geschrieben += 1
            print("Neues Backup für präventive Bans geschrieben")
            return zeilen_geschrieben
        print("Kein Backup für präventive Bans geschrieben")
        return 0

    def rename_backup(self) -> None:
        """Benennt Original-Datei um. Fängt sehr ungewöhnliche Exceptions ab, weil Crash an dieser Stelle sehr
        unerwünscht ist.
        Verlängert im Fall einer Exception den Zielnamen um die aktuellen Zehntel-Sekunden von utcnow"""
        if os.path.isfile(self.FILENAME_BACKUP):
            original_name = os.getcwd() + os.path.sep + self.FILENAME_BACKUP
            target_name = f"{original_name}_{datetime.utcnow().__str__()[:19].replace(':','')}"
            try:
                os.rename(original_name, target_name)
            except FileExistsError:
                # Should be raised only on windoze
                print(texts.console__very_unlikely__filename_for_renaming_backup_used_by_file)
                try:
                    target_name = f"{original_name}_{datetime.utcnow().__str__()[:19].replace(':','')}"
                    os.replace(original_name, target_name)
                except OSError:
                    print(texts.console__very_very_unlikely__OSError_after_FileExistsError)
            except IsADirectoryError or OSError:
                print(texts.console__very_unlikely__filename_for_renaming_backup_used_by_dir)
                target_name = f"{original_name}_{datetime.utcnow().__str__()[:19].replace(':','')}"
                os.replace(original_name, target_name)
